# Cesta k registrům
$RegistryPath = "HKLM:\SOFTWARE\Microsoft\Windows Photo Viewer\Capabilities\FileAssociations"

# Výchozí aplikace
$Value = "PhotoViewer.FileAssoc.Tiff"

# Typy souboru
$FileTypes = @(".bmp",".dib",".gif",".jfif",".jpe",".jpeg",".jxr",".png")

foreach ($FileType in $FileTypes)
    {
        New-ItemProperty -Path $RegistryPath -Name $FileType -Value $Value -PropertyType STRING -Force | Out-Null
    }