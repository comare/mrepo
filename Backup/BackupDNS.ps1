﻿##########################################################################################################
# Script for DNS backup
# Run localy and store data on remote storage
##########################################################################################################
#Get Name of the server and domain with env variable
$DnsServer = $env:computername
#$DnsDomain = (Get-ADDomain -Current LocalComputer).name

#Remote direcotry - set or create if not exist
$RemoteDir = "\\ha-ntc.ics.muni.cz\winadm\backups\DNS\$DnsServer"
if (!(Test-Path -Path $RemoteDir)) {
    New-Item -ItemType directory -Path $RemoteDir
}

#Define date / time variable
$DateTime = Get-Date -Format yy_MM_dd_HHmmss

#Define folder where to store backup
$BckPath =”c:\windows\system32\dns\backup\$DateTime"

#Create backup folder
$Create = New-Item -Path $BckPath -ItemType Directory -ErrorAction SilentlyContinue

Start-Transcript -Path "$BckPath\logfile.log"

if ($Create) {

    #Define file name for Dns Settings
    $File = Join-Path $BckPath “input.csv”


    #Get DNS settings using WMI
    $List = Get-WmiObject -ComputerName $DnsServer -Namespace root\MicrosoftDNS -Class MicrosoftDNS_Zone

    if ($List) {

        #Export information into input.csv file
        $List | Select-Object Name,ZoneType,AllowUpdate,@{Name=”MasterServers”;Expression={$_.MasterServers}},DsIntegrated | Export-csv $File -NoTypeInformation


        #Call Dnscmd.exe to export dns zones
         $List | ForEach-Object {

             $ZonePath = ”backup\$($DateTime)\$($_.Name).dns"
             &"C:\Windows\system32\dnscmd.exe" $DnsServer `/ZoneExport $_.Name $ZonePath
 
         }   #end of ForEach-Object

        #Move files to remote directory
        Write-Host "Moving files to $RemoteDir"
        Stop-Transcript
        Move-Item -Path $BckPath -Destination $RemoteDir

    }   #end of if ($List)
    else {

    Write-Error "Failed to obtain DNS WMI object... exiting script"

    }   #end of else ($List)

}   #end of if ($Create)
else {

    Write-Error "Failed to create backup folder... exiting script"

}   #end of else ($Create)