﻿#***********************************
# Script for GPO backup ************
#***********************************
#Get Name of the server and domain with env variable
$DnsServer = $env:computername
$DnsDomain = (Get-ADDomain -Current LocalComputer).name

#Remote direcotry - set or create if not exist
$RemoteDir = "\\ha-ntc.ics.muni.cz\winadm\backups\GPO\$DnsDomain"
#$RemoteDir = "C:\GpoBackupRemote"
if (!(Test-Path -Path $RemoteDir)) {
    New-Item -ItemType directory -Path $RemoteDir
}

#Localdir - local folder for backup and compress
$Localdir = "C:\GpoBackups"
if (!(Test-Path $Localdir)) {
    New-Item -ItemType Directory -Force -Path $Localdir
}

#Define actualtime variable
$DateTime = Get-Date -Format yy_MM_dd_HHmmss

#Start of transcript in destination folder
Start-Transcript -Path "$RemoteDir\$DnsDomain-$DateTime-logfile.log"

#Define compressed file name variable
$CommpressedGPO = "$Localdir\$DnsDomain-$DateTime.zip"

#Save local GPO to Localdir on server
Backup-Gpo -All -Path $Localdir

#Zip saved local GPOs to one file
Compress-Archive -Path $Localdir\* -DestinationPath $CommpressedGPO

#Move zipped file and log to RemoteDir
Move-Item -Path $CommpressedGPO -Destination $RemoteDir

#Remove all files and folders from Localdir variable
Remove-Item –path $Localdir\* -recurse -force -confirm:$false

#End transkript
Stop-Transcript