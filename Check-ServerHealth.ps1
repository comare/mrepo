# Check services that should be runnig and are stopped
$command = Get-service | Where-Object Status -eq "Stopped" | Where-Object StartType -eq "Automatic"

# Get latest logs from provider time service
$command = Get-WinEvent -ProviderName Microsoft-Windows-Time-Service -MaxEvents 10

# Get latest critical logs from replication
$command = Get-EventLog -LogName 'DFS Replication' -Newest 20 -EntryType Error | Format-Table -Wrap

# Invoke skript on computer or set of computers
[string[]]$computers = "fss-server1", "Fss-server2", "Fss-dctemp.fss.ucn.muni.cz"
Invoke-Command -ComputerName $computers -ScriptBlock {$Using:command}