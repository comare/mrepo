﻿# Setup variables for envirment
$DnsServer = "dtest-dc1.dtest.muni.cz"  
$ForwardLookupZone = "dtest.muni.cz"

# Get all reverse lookup zones / used one more times in code
function Get-ReverseLookupZones {
    Get-DnsServerZone -ComputerName $DnsServer | Where-Object {$_.IsReverseLookupZone -eq $true -and $_.IsAutoCreated -eq $false}
}

$ReverseLookupZones = Get-ReverseLookupZones

# Get all relevant records from forward lookup zone
$DNSAresources = Get-DnsServerResourceRecord -ZoneName $ForwardLookupZone -RRType A -ComputerName $DnsServer | Where-Object {$_.Hostname -ne "@" -and $_.Hostname -ne "DomainDnsZones" -and $_.Hostname -ne "ForestDnsZones"}

foreach ($DNSA in $DNSAresources) {
    # Split ip address of record to array
    $DNSA_IPaddr = $DNSA.RecordData.IPv4Address.IPAddressToString
    $DNSA_AddrSplit = ($DNSA_IPaddr -split "\.")
    
    # Get IP address and name of reverse zone from splitted ip
    $ReverseZoneAddr = $DNSA_AddrSplit[2]+"."+$DNSA_AddrSplit[1]+"."+$DNSA_AddrSplit[0]+".in-addr.arpa"
    $ReverseZoneIP = $DNSA_AddrSplit[0]+"."+$DNSA_AddrSplit[1]+"."+$DNSA_AddrSplit[2]+".0/24"
    
    # Get PTR record from splitted ip
    $DNSPtr = $DNSA_AddrSplit[3]
    
    Write-Output("Checking record - " + $DNSA.HostName)
    
    # Is there zone for actual ip in dns server / if not write it
    if ($ReverseLookupZones.ZoneName -notcontains $ReverseZoneAddr) {
        Write-Output ("NEW REVERSE ZONE writting - " + $ReverseZoneIP)
        Add-DnsServerPrimaryZone -NetworkID $ReverseZoneIP -ReplicationScope "Domain"
        $ReverseLookupZones = Get-ReverseLookupZones
    }
    
    # Is there existing record in reverse DNS for actual IP / yes print / no add record
    if (Get-DnsServerResourceRecord -ZoneName $ReverseZoneAddr -Name $DNSPtr -ErrorAction SilentlyContinue) {
        Write-Output ("*****Existing reverse record - " + $DNSPtr +" (" + $DNSA_IPaddr + ")" + " in reverse zone " + $ReverseZoneAddr)
    } else {
        Write-Output ("WRITING PTR - " + $DNSPtr +" (" + $DNSA_IPaddr + ")" + " in reverse zone " + $ReverseZoneAddr)
        $RecordHostName = $DNSA.HostName + "." + $ForwardLookupZone
        Add-DnsServerResourceRecordPtr -Name $DNSPtr -ZoneName $ReverseZoneAddr -AllowUpdateAny -AgeRecord -TimeToLive 01:00:00 -PtrDomainName $RecordHostName
    }
}
