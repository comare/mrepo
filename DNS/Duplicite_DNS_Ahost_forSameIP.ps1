﻿Start-Transcript
$records = Get-DnsServerResourceRecord -ComputerName zam-server1 -ZoneName "zam.ucn.muni.cz" -RRType A
foreach ($record in $records) {
    $IP = $record.RecordData.IPv4Address.IPAddressToString
    $Hostname = $record.HostName
    if (($Hostname  -ne "@") -and ($Hostname -ne "DomainDnsZones")) {
    
        foreach ($testRecord in $records) {
            $testIP = $testRecord.RecordData.IPv4Address.IPAddressToString
            $testHostname = $testRecord.HostName
            if (($testIP -eq $IP) -and ($testHostname -ne $Hostname)) {
                if (($testHostname  -ne "@") -and ($testHostname -ne "DomainDnsZones")) {
                    Write-Host "*******Found same IP for 2 different HOSTs***********************"
                    Write-Host $Hostname - $record.Timestamp - $IP
                    Write-Host $testHostname - $testRecord.Timestamp - $testIP
                }
            }
        }
    }
}
Stop-Transcript