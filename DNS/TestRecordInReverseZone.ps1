﻿# Setup variables for envirment
 $DnsServer = "dtest-dc1"  
 $ForwardLookupZone = "dtest.muni.cz"  
 #$ReverseLookupZone = "71.251.147.in-addr.arpa"
 $ReverseLookupZone = "1.168.192.in-addr.arpa"
 ################################  
 # Test missing PTR record in specific Reverse Zone
 ###############################  
 $DNSAresources = Get-DnsServerResourceRecord -ZoneName $ForwardLookupZone -RRType A -ComputerName $DnsServer | Where-Object {$_.Hostname -ne "@" -and $_.Hostname -ne "DomainDnsZones" -and $_.Hostname -ne "ForestDnsZones"}  
 foreach ($DnsA in $DNSAresources) {  
   $DNSPtr = ($DNSA.RecordData.IPv4Address.IPAddressToString -split "\.")[3]  
   $DNSPtrRecord = Get-DnsServerResourceRecord -ZoneName $ReverseLookupZone -Name $DNSPtr -RRType Ptr -ErrorAction SilentlyContinue  
   if ($DNSPtrRecord -eq $null) {  
     $DNSPtrRecord = New-Object psobject  
     $DNSPtrRecord | Add-Member -Name Hostname -MemberType NoteProperty -Value "NotExist"  
     $record = New-Object psobject  
     $record | Add-Member -Name PtrDomainName -MemberType NoteProperty -Value "NotExist"  
     $DNSPtrRecord | Add-Member -Name RecordData -MemberType NoteProperty -Value $record  
   }#if  
   else {  
     $PTRHostname = "{0}.{1}." -f $DnsA.HostName, $ForwardLookupZone  
     $DNSPtrRecord = $DNSPtrRecord | Where-Object {$_.RecordData.PtrDomainName -Match $PTRHostname}  
     if ($DNSPtrRecord -eq $null) {  
       $DNSPtrRecord = New-Object psobject  
       $DNSPtrRecord | Add-Member -Name Hostname -MemberType NoteProperty -Value "NotExist"  
       $record = New-Object psobject  
       $record | Add-Member -Name PtrDomainName -MemberType NoteProperty -Value "NotExist"  
       $DNSPtrRecord | Add-Member -Name RecordData -MemberType NoteProperty -Value $record  
     }#$if  
   }#else  
   $Obj = New-Object psobject  
   $Obj | Add-Member -Name Hostname -MemberType NoteProperty -Value $DnsA.Hostname  
   $Obj | Add-Member -Name IP -MemberType NoteProperty -Value $DnsA.Recorddata.IPv4Address.IPAddressToString  
   $Obj | Add-Member -Name DNSPtrRecord -MemberType NoteProperty -Value $DNSPtrRecord.Hostname  
   $Obj | Add-Member -Name PTRHostName -MemberType NoteProperty -Value $DNSPtrRecord.RecordData.PtrDomainName  
   $Obj   
 }#foreach