﻿workflow Install-WinRDS {
    # Add features and import modules
    Add-WindowsFeature –Name RDS-RD-Server –IncludeAllSubFeature -Restart
    Import-Module RemoteDesktop

    #Install RDS
    New-RDSessionDeployment –ConnectionBroker ics-server-gw.ics.ad.muni.cz –WebAccessServer ics-server-gw.ics.ad.muni.cz –SessionHost ics-server-gw.ics.ad.muni.cz

    # Setup licencing services
    Add-RDServer -Server ics-server-gw.ics.ad.muni.cz -Role RDS-LICENSING -ConnectionBroker ics-server-gw.ics.ad.muni.cz
    Set-RDLicenseConfiguration -LicenseServer ics-server-gw.ics.ad.muni.cz -Mode PerUser -ConnectionBroker ics-server-gw.ics.ad.muni.cz

    # Create session host collection
    New-RDSessionCollection -CollectionName "SessionGW" -SessionHost @("RDSH01.Contoso.com"," RDSH02.Contoso.com") -CollectionDescription "Session collection for West office hosts." -ConnectionBroker "RDCB.Contoso.com"

    # Add new app to session collection
    New-RDRemoteApp -CollectionName "Session Collection 01" -DisplayName "Notepad" -FilePath "C:\Windows\System32\Notepad.exe"
}

Install-WinRDS -Verbose