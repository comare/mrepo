﻿$N1_session = New-PSSession -ComputerName GIS-NLB-TEST-N1.dis.ics.muni.cz
$N2_session = New-PSSession -ComputerName GIS-NLB-TEST-N2.dis.ics.muni.cz

Invoke-Command -Session $N1_session, $N2_session -ScriptBlock {Install-WindowsFeature NLB -IncludeManagementTools}

Invoke-Command -Session $N1_session -ScriptBlock {
    New-NlbCluster -InterfaceName “Ethernet” -OperationMode Multicast -ClusterPrimaryIP 147.251.18.93 -SubnetMask 255.255.255.224 -ClusterName GIS-NLB-TEST
    Add-NlbClusterNode -InterfaceName “Ethernet” -NewNodeName “GIS-NLB-TEST-N2” –NewNodeInterface “Ethernet”
}

Get-PSSession | Remove-PSSession