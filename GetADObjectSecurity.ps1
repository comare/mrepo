﻿<#
$DN = (Get-ADUser -Identity 143208).DistinguishedName
cd AD:
(Get-Acl $DN).access | select identityreference, ActiveDirectoryRights, InheritanceType, accesscontroltype | Sort-Object identityreference | Format-Table -Wrap -AutoSize
#>

# Get-ADUser -server "ucn-server0.ucn.muni.cz" -filter {name -like 143544} -Properties houseIdentifier



$start = 1
$end = 600000

Write-Output "Listing users from $start to $end who have houseIdentifier visible:"

for ($i=$start; $i -le $end; $i++) {
    $user = Get-ADUser -server "ucn-server0.ucn.muni.cz" -filter {name -like $i} -Properties houseIdentifier, Enabled
    if ($i % 50000 -eq 0) {
        Write-Output "Now at UCO number $i"            
    }
    if ($user.Enabled -eq "True") {
        if ($user.houseIdentifier -ne "") {
            Write-Output "$($user.Name) - $($user.houseIdentifier) - $($user.Enabled) - $($user.GivenName) $($user.Surname)"
        }
    }
}