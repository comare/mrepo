
$secpasswd = ConvertTo-SecureString "J3imzaSI7cvmITWKQOsHRt9BZfxuN7Lx" -AsPlainText -Force
$mycreds = New-Object System.Management.Automation.PSCredential ("icinga-api-kotlik", $secpasswd)
#$url = "https://icinga-demo.ics.muni.cz:5665/v1/objects/notifications/tethys.dis.ics.muni.cz!Memory1!winadm-service-down"
#$url = "https://icinga-demo.ics.muni.cz:5665/v1/objects/hosts/law-server1.law.ucn.muni.cz"
#$url = "https://monitor.ics.muni.cz:5665/v1/objects/hostgroups?filter=match(%22win*%22,hostgroup.name)"
$url = "https://monitor.ics.muni.cz:5665/v1/objects/users/dis-sluzba@ics.muni.cz"

$header = @{"Accept"="application/json"}

#######################################################################################
#Get Object
$content = Invoke-RestMethod -Uri $url -Method Get -Credential $mycreds

#Remove Object
#$content = Invoke-RestMethod -Uri $url -Method DELETE -Credential $mycreds -Headers $header

Write-Output $content.results
Write-Output $content.results.attrs.last_check_result
