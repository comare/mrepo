#Requires -Version 4.0
######################################################################
# Domain specific variables DIS
#$installfromdir = "\\dis.ics.muni.cz\network\software\remoteinstall\NSClient++\0.5.2"
#$installfromdirconfig = "\\dis.ics.muni.cz\network\software\remoteinstall\NSClient++\0.5.2\config"

# Domain specific variables UCN
#$installfromdir = "\\sccm-01.ucn.muni.cz\PackageSource\SW distributions\NSClient++\0.5.2"
$installfromdir = "\\sccm-01.ucn.muni.cz\NSClient++\0.5.2"
$installfromdirconfig = "\\sccm-01.ucn.muni.cz\monitoring\servers"

# Domain specific variables ICS
#$installfromdir = "\\ics.ad.muni.cz\network\aplikace\Install\RemoteInstall\NSClient++\0.5.2"
#$installfromdirconfig = "\\ics.ad.muni.cz\network\aplikace\Install\RemoteInstall\NSClient++\0.5.2\config"

#Local install with remote PS Session from script NSC_Install_localy.ps1
#If argument is set, use it ad install from direstory
if ($null -ne $args) {
    $installfromdir = $args[0]
    $installfromdirconfig = $args[1]
}
#>
######################################################################
$installtodir = "$env:ProgramFiles\NSClient++"
$serverName = $env:COMPUTERNAME
$backupdir = "C:\NSClient_old_backup"
$newNscFile = "NSCP-0.5.2.39-x64.msi"
$newNscVersion = "0.5.2.39"

Start-Transcript -Path "$($installfromdir)\logs\$($serverName)_transcript.log" 

Write-Output "Paths for remote install and config folders $installfromdir and $installfromdirconfig" -ErrorAction SilentlyContinue

$nscInstalled = Get-ItemProperty HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\* |
    Select-Object DisplayName, DisplayVersion, InstallDate, PSChildName  | 
    Where-Object DisplayName -like "*NSClient*"
######################################################################
# If old version of nsclient is installed - uninstall it.
if (Get-Service "*nsclient*") {
    
}
# If new version of nsclient isn't installed - install it.
if (-not(Get-Service "*nscp*")) {

}

# If nsclient is installed and his version is lower than new one, than uninstall it
if (($nscInstalled) -and ($nscInstalled.DisplayVersion -lt $newNscVersion)) {
    Write-Output "Stopping NSClient old service"
    Get-service  -Name "nsclient*" | Stop-Service

    # Make backup of config and scripts from old script
    New-Item -ItemType Directory -Path $backupdir
    Copy-Item -Path "$installtodir\*.ini" -Destination $backupdir -Force
    Copy-Item -Path "$installtodir\scripts\*" -Destination "$backupdir\scripts\" -Recurse -Force

    Write-Output "Uninstalling version $($nscInstalled.DisplayVersion)"
    $args = "/X$($nscInstalled.PSChildName) /qn REBOOT=REALLYSUPPRESS"
    Start-Process msiexec.exe -ArgumentList $args -PassThru | Wait-Process
    Write-Output "Uninstall of version $($nscInstalled.DisplayVersion) complete"
    
    # Delete rest of the files in old installation folder
    Write-Output "Deleting rest of old files"
    Remove-Item -Path $installtodir -Recurse -Force
}

# If nsclient is NOT installed OR his version WAS lower than new one, than install it
if ((-not($nscInstalled)) -or ($nscInstalled.DisplayVersion -lt $newNscVersion)) {
    Write-Output "Installing version $newNscVersion"
    $args2 = "/i `"$($installfromdir)\$newNscFile`" /qn REBOOT=REALLYSUPPRESS"
    Start-Process msiexec.exe -ArgumentList $args2 -PassThru | Wait-Process
    Write-Output "Install of version $newNscVersion complete"
}

# Configure NSClient - specific for some servers
if (Get-Service "*nscp*") {
    Write-Output "Stopping nscp service"
    Stop-Service -Name nscp

    # copy custom or default scripts and config for server
    $customServers = Get-ChildItem -Path $installfromdirconfig -Directory | Select-Object -ExpandProperty Name
    if ($customServers -contains $serverName) {
        Write-Output "Copying $serverName config file to server"
        Copy-Item -Path "$($installfromdirconfig)\$($serverName)\*" -Destination "$installtodir\" -Recurse -Force
    } else {
        Write-Output "Copying default config file to server"
        Copy-Item -Path "$installfromdirconfig\default\*" -Destination "$installtodir\" -Recurse -Force
    }
    
    Write-Output "Starting nscp service"
    Start-Service -Name nscp
}
Stop-Transcript -ErrorAction SilentlyContinue