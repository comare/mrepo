[string[]]$computersList = "Q5-CML-MU","Q5-CML-PED","Q5-CML-PHIL","q5-cml-ukbA26","Q5-KREDIT-MU","Q5-MOB-MU","q5-ocr-phil","SafeQDB"
[string]$source_folder = "\\sccm-01.ucn.muni.cz\NSClient++\0.5.2"
[string]$source_config = "\\sccm-01.ucn.muni.cz\monitoring\servers"

[string]$remote_install_folder = "C:\NSClient_install"
[string]$remote_install_config = "C:\NSClient_install\config"

ForEach ($computerName in $computersList) {
    Start-Transcript -Path "$($source_folder)\logs\$($computerName)_transcript.log" -Force
    $session = New-PSSession -ComputerName $computerName
    Invoke-Command -Session $session -ScriptBlock {New-Item -ItemType Directory -Path $args[0]; New-Item -ItemType Directory -Path $args[1]} -ArgumentList $remote_install_folder, $remote_install_config
    Copy-Item -Path "$($source_folder)\*" -ToSession $session -Destination $remote_install_folder -Recurse -Exclude "$($source_folder)\logs\*" -Force
    Copy-Item -Path "$($source_config)\*" -ToSession $session -Destination $remote_install_config -Recurse -Force

    Invoke-Command -Session $session -FilePath "$($source_folder)\NSC_install.ps1" -ArgumentList $remote_install_folder, $remote_install_config
    #Copy-Item -FromSession $session -Path "$($remote_install_folder)\logs\$($computerName)_transcript.log" -Destination "$($source_folder)\logs" -Force
    Invoke-Command -Session $session -ScriptBlock {Remove-Item -Path $args[0] -Recurse -Force} -ArgumentList $remote_install_folder
    Remove-PSSession $session
    Stop-Transcript
}
