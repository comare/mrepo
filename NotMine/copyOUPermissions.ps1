﻿$sourcePath = 'AD:ou=rcx,ou=ucebny,OU=Win10,OU=Infrastruktura_SCCM,DC=ups,DC=ucn,DC=muni,DC=cz'
$destPath = 'AD:ou=rcx,ou=studovny,OU=Win10,OU=Infrastruktura_SCCM,DC=ups,DC=ucn,DC=muni,DC=cz'

Import-Module ActiveDirectory

$sourceACL = Get-Acl -Path $sourcePath
$destACL = Get-Acl -Path $destPath


# You might want to clear the destination ACL of non-inherited ACEs before starting this loop

foreach ($sourceACLRule in $sourceACL.Access)
{
    if ($sourceACLRule.IsInherited) { continue }
    "---------- SOURCE ACL v ----------"
    $sourceACLRule
    $identityReference = $null

#    try
#    {
        $sourceAccount = $sourceACLRule.IdentityReference.Translate([System.Security.Principal.NTAccount])
        $newName = $sourceAccount.Value -replace $sourceACL.Name, $destACL.Name
        $identityReference = [System.Security.Principal.NTAccount]$newName
#    }
#    catch
#    {
        # You may want to log an error if you can't translate the SID to User\Group form
#    }

    $destACLRule = $destACL.AccessRuleFactory($identityReference,
                                            $sourceACLRule.ActiveDirectoryRights,
                                            $sourceACLRule.IsInherited,
                                            $sourceACLRule.InheritanceFlags,
                                            $sourceACLRule.PropagationFlags,
                                            $sourceACLRule.AccessControlType,
                                            $sourceACLRule.ObjectType,
                                            $sourceACLRule.InheritedObjectType)

    "---------- DESTINATION ACL v ----------"
    $destACLRule 

    # This statement will throw an error if the destination identity doesn't exist.
    $destACL.AddAccessRule($destACLRule)
    
}
Set-Acl -Path $destPath -AclObject $destACL -Passthru -Verbose 
