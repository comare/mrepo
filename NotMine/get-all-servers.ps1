﻿
$doms = (Get-ADForest).Domains
$s = hostname
$out = ".\allServers_$((Get-Date).ToString('dd-MM-yyyy')).csv"
rm $out

foreach ($d in $doms) {
    Get-ADComputer -Filter { OperatingSystem -like "*Windows Server*"} -Properties LastLogonDate, IPv4Address, OperatingSystem -Server $d `
        | select-object -Property @{l="Domain"; e={$d}}, Name, DNSHostName, @{Name=”LogonDate”;Expression={$_.LastLogonDate.ToString(“dd-MM-yyyy”)}}, DistinguishedName, IPv4Address, OperatingSystem `
        | export-csv $out -NoTypeInformation -Append
}