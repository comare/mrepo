﻿######################################
#Operating System Discovery In Domain#
######################################

$outputFile = "C:\Skripty\OS_Rep_anniversary.txt"

New-Item $outputFile -type file -force

#Computers in these OUs are considered "student" PCs. All the others are considered "employee" PCs (except for servers).
Function IsStudent {
  Param(
  [string]$dname
  )

  #The entire OU "INFRASTRUKTURA" in the root of ups.ucn.muni.cz
  Process{
  if ($dname -like "*OU=INFRASTRUKTURA,DC=ups*")
  {
    return $true
  }

  #OUs named "J151 a 152 w7", "PC25 w7" and "PC26 w7" in fss.ucn.muni.cz
  if ($dname -like "*DC=fss*")
  {
    if (($dname -like "*OU=J151 a J152 w7*") -or ($dname -like "*OU=PC25 w7*") -or ($dname -like "*OU=PC26 w7*"))
    {
      return $true
    }
  }

  #The entire OU named "LAW" in the root of law.ucn.muni.cz
  if ($dname -like "*OU=LAW,DC=law*")
  {
    return $true
  }

  #The entire OU named "PHIL" in the root of phil.ucn.muni.cz
  if ($dname -like "*OU=PHIL,DC=phil*")
  {
    return $true
  }

  #OU "Studentske" (INFRASTRUKTURA/Poslucharny/Studentske) in zam.ucn.muni.cz
  if ($dname -like "*OU=Studentske,OU=Poslucharny,OU=INFRASTRUKTURA,DC=zam*")
  {
    return $true
  }

  return $false
  }
}

#I'm sorry
$StudentPC_1511_total = 0
$StudentPC_1607_total = 0
$StudentPC_vintage_total = 0
$StudentPC_other_total = 0
$WorkPC_1511_total = 0
$WorkPC_1607_total = 0
$WorkPC_vintage_total = 0
$WorkPC_other_total = 0

(Get-ADForest).Domains | foreach {

    $computers = get-adcomputer -Server $_ -Filter 'ObjectClass -eq "Computer"' -properties OperatingSystem,OperatingSystemVersion,DistinguishedName -SearchScope Subtree

    #I'm SO sorry
    $StudentPC_1511 = 0
    $StudentPC_1607 = 0
    $StudentPC_vintage = 0
    $StudentPC_other = 0
    $WorkPC_1511 = 0
    $WorkPC_1607 = 0
    $WorkPC_vintage = 0
    $WorkPC_other = 0

    Foreach ($computer in $computers) {
        if ($computer.OperatingSystem -notlike "*Windows 10*") { # Other operating systems (not Windows 10)
            if (IsStudent -dname $computer.DistinguishedName)
            {
              $StudentPC_other++
              $StudentPC_other_total++
            }
            else
            {
              $WorkPC_other++
              $WorkPC_other_total++
            }

            }
        elseif ($computer.OperatingSystemVersion -like "*10586*") { # Version 1511 (build number: 10586)
            if (IsStudent -dname $computer.DistinguishedName)
            {
              $StudentPC_1511++
              $StudentPC_1511_total++
            }
            else
            {
              $WorkPC_1511++
              $WorkPC_1511_total++
            }

            #Uncomment this to create a list of PCs that have version 1511 installed.
            #New-Item C:\Skripty\1511_PCs.txt -type file -force
            $name = $computer.DistinguishedName
            "$name" | Out-file -append C:\Skripty\1511_PCs.txt

            }
	    elseif ($computer.OperatingSystemVersion -like "*14393*") { # Version 1607 (build number: 14393)
            if (IsStudent -dname $computer.DistinguishedName)
            {
              $StudentPC_1607++
              $StudentPC_1607_total++
            }
            else
            {
              $WorkPC_1607++
              $WorkPC_1607_total++
            }

            }
        else { # Other versions of Windows 10

        #Uncomment this to create a list of the other build numbers.
        #New-Item C:\Skripty\other_builds.txt -type file -force
        #$OSV = $computer.OperatingSystemVersion
        #"$OSV" | Out-file -append C:\Skripty\other_builds.txt

            if (IsStudent -dname $computer.DistinguishedName)
            {
              $StudentPC_vintage++
              $StudentPC_vintage_total++
            }
            else
            {
              $WorkPC_vintage++
              $WorkPC_vintage_total++
            }
            
            }
    }
    "#############################" | Out-file -append $outputFile
    "##  OS    $_ ##" | Out-file -append $outputFile
    "#############################" | Out-file -append $outputFile
    "$WorkPC_1511 --> 1511 (employee PCs)" | Out-file -append $outputFile
    "$StudentPC_1511 --> 1511 (student PCs)" | Out-file -append $outputFile
    "$WorkPC_1607 --> 1607 (employee PCs)" | Out-file -append $outputFile
    "$StudentPC_1607 --> 1607 (student PCs)" | Out-file -append $outputFile
    "$WorkPC_vintage --> Other versions of Win10 (employee PCs)" | Out-file -append $outputFile
    "$StudentPC_vintage --> Other versions of Win10 (student PCs)" | Out-file -append $outputFile
    "$WorkPC_other --> Other OS (employee PCs)" | Out-file -append $outputFile
    "$StudentPC_other --> Other OS (student PCs)" | Out-file -append $outputFile
    "---------------------------------------------" | Out-file -append $outputFile
    "                                             " | Out-file -append $outputFile
}

"##########################################################" | Out-file -append $outputFile
"##########################################################" | Out-file -append $outputFile
"                                                          " | Out-file -append $outputFile
"#############################" | Out-file -append $outputFile
"##  DOMAIN TOTALS  ##" | Out-file -append $outputFile
"#############################" | Out-file -append $outputFile
"$WorkPC_1511_total --> 1511 (employee PCs)" | Out-file -append $outputFile
"$StudentPC_1511_total --> 1511 (student PCs)" | Out-file -append $outputFile
"$WorkPC_1607_total --> 1607 (employee PCs)" | Out-file -append $outputFile
"$StudentPC_1607_total --> 1607 (student PCs)" | Out-file -append $outputFile
"$WorkPC_vintage_total --> Other versions of Win10 (employee PCs)" | Out-file -append $outputFile
"$StudentPC_vintage_total --> Other versions of Win10 (student PCs)" | Out-file -append $outputFile
"$WorkPC_other_total --> Other OS (employee PCs)" | Out-file -append $outputFile
"$StudentPC_other_total --> Other OS (student PCs)" | Out-file -append $outputFile
"---------------------------------------------" | Out-file -append $outputFile
"                                             " | Out-file -append $outputFile