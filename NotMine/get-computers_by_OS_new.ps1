﻿######################################
#Operating System Discovery In Domain#
######################################

$outputFile = "C:\Skripty\OS_Rep2018.txt"

New-Item $outputFile -type file -force

#Computers in these OUs are considered "student" PCs. All the others are considered "employee" PCs (except for servers).
Function IsStudent {
  Param(
  [string]$dname
  )

  #The entire OU "INFRASTRUKTURA" in the root of ups.ucn.muni.cz
  Process{
  if (($dname -like "*OU=INFRASTRUKTURA_SCCM,DC=ups*") -or ("*OU=INFRASTRUKTURA,DC=ups*"))
  {
    return $true
  }

  #OUs named "J151 a 152 w7", "PC25 w7" and "PC26 w7" and "j049a w7" in fss.ucn.muni.cz
  if ($dname -like "*DC=fss*")
  {
    if (($dname -like "*OU=Knihovna*") -or ($dname -like "*OU=PC25*") -or ($dname -like "*OU=j227*") -or ($dname -like "*OU=j228*"))
    {
      return $true
    }
  }

  #The entire OU named "INFRASTRUKTURA_SCCM" in the root of law.ucn.muni.cz
  if ($dname -like "*OU=INFRASTRUKTURA_SCCM,DC=law*")
  {
    return $true
  }

  #The entire OU named "INFRASTRUKTURA_SCCM\PHIL-W10" in the root of phil.ucn.muni.cz
  if ($dname -like "*OU=PHIL-W10,OU=INFRASTRUKTURA_SCCM,DC=phil*")
  {
    return $true
  }
  
  #The entire OU named "PHIL" in the root of phil.ucn.muni.cz
  if ($dname -like "*OU=PHIL,DC=phil*")
  {
    return $true
  }
  #OU "Studentske" (INFRASTRUKTURA/Poslucharny/Studentske) in zam.ucn.muni.cz
 # if ($dname -like "*OU=Studentske,OU=Poslucharny,OU=INFRASTRUKTURA,DC=zam*")
 # {
 #   return $true
 # }
  if ($dname -like "*DC=zam*")
  {
    if (($dname -like "*OU=Studentske,OU=CZ,OU=ROOM,OU=DOMAIN,OU=INFRASTRUKTURA_SCCM,DC=zam*") -or ($dname -like "*OU=Studentske,OU=EN,OU=ROOM,OU=DOMAIN,OU=INFRASTRUKTURA_SCCM,DC=zam*"))
    {
      return $true
    }
  }

  return $false
  }
}

#I'm sorry
$Win7_total = 0
$WinXP_total = 0
$Win8_total = 0
$Win10_total = 0
$Win2000Pro_total = 0
$WinVista_total = 0
$Win2000Server_total = 0
$WS2003_total = 0
$WS2008_total = 0
$WS2008R2_total = 0
$WS2012_total = 0
$WS2012R_total = 0
$StudentPC_total = 0
$StudentPC_10_total = 0
$WorkPC_total = 0
$WorkPC_10_total = 0
$Server_total = 0

(Get-ADForest).Domains | foreach {

    $computers = get-adcomputer -Server $_ -Filter 'ObjectClass -eq "Computer"' -properties OperatingSystem,DistinguishedName -SearchScope Subtree

    #I'm SO sorry
    $Win7 = 0
    $WinXP = 0
    $Win8 = 0
    $Win10 = 0
    $Win2000Pro = 0
    $WinVista = 0
    $Win2000Server = 0
    $WS2003 = 0
    $WS2008 = 0
    $WS2008R2 = 0
    $WS2012 = 0
    $WS2012R2 = 0
    $StudentPC = 0
    $StudentPC_10 = 0
    $WorkPC = 0
    $WorkPC_10 = 0
    $Server = 0

    Foreach ($computer in $computers) {
        if ($computer.OperatingSystem -like "*Windows 7*") {
            $Win7++
            $Win7_total++

            if (IsStudent -dname $computer.DistinguishedName)
            {
              $StudentPC++
              $StudentPC_total++
            }
            else
            {
              $WorkPC++
              $WorkPC_total++
            }

            }
        elseif ($computer.OperatingSystem -like "*Windows XP*") {
            $WinXP++
            $WinXP_total++

            if (IsStudent -dname $computer.DistinguishedName)
            {
              $StudentPC++
              $StudentPC_total++
            }
            else
            {
              $WorkPC++
              $WorkPC_total++
            }

            }
	    elseif ($computer.OperatingSystem -like "*Windows 8*") {
            $Win8++
            $Win8_total++

            if (IsStudent -dname $computer.DistinguishedName)
            {
              $StudentPC++
              $StudentPC_total++
            }
            else
            {
              $WorkPC++
              $WorkPC_total++
            }

            }
        elseif ($computer.OperatingSystem -like "*Windows 10*") {
            $Win10++
            $Win10_total++

            if (IsStudent -dname $computer.DistinguishedName)
            {
              $StudentPC++
              $StudentPC_total++
              $StudentPC_10++
              $StudentPC_10_total++
            }
            else
            {
              $WorkPC++
              $WorkPC_total++
              $WorkPC_10++
              $WorkPC_10_total++
            }

            }
	    elseif ($computer.OperatingSystem -like "*Windows 2000 Professional*") {
            $Win2000Pro++
            $Win2000Pro_total++            

            if (IsStudent -dname $computer.DistinguishedName)
            {
              $StudentPC++
              $StudentPC_total++
            }
            else
            {
              $WorkPC++
              $WorkPC_total++
            }

            }
	    elseif ($computer.OperatingSystem -like "*Windows Vista*") {
            $WinVista++
            $WinVista_total++

            if (IsStudent -dname $computer.DistinguishedName)
            {
              $StudentPC++
              $StudentPC_total++
            }
            else
            {
              $WorkPC++
              $WorkPC_total++
            }

            }
	    elseif ($computer.OperatingSystem -like "*Windows 2000 Server*") {
            $Win2000Server++
            $Win2000Server_total++
            $Server++
            $Server_total++
            }
	    elseif ($computer.OperatingSystem -like "*Windows Server 2003*") {
            $WS2003++
            $WS2003_total++
            $Server++
            $Server_total++
            }
	    elseif ($computer.OperatingSystem -like "*Windows Server 2008 R2*") {
            $WS2008R2++
            $WS2008R2_total++
            $Server++
            $Server_total++
            }
	    elseif ($computer.OperatingSystem -like "*Windows Server 2008*") {
            $WS2008++
            $WS2008_total++
            $Server++
            $Server_total++
            }
	    elseif ($computer.OperatingSystem -like "*Windows Server 2012 R2*") {
            $WS2012R2++
            $WS2012R2_total++
            $Server++
            $Server_total++
            }
	    elseif ($computer.OperatingSystem -like "*Windows Server 2012*") {
            $WS2012++
            $WS2012_total++
            $Server++
            $Server_total++
            }
	    else {
            $_
            }

    }
    "#############################" | Out-file -append $outputFile
    "##  OS    $_ ##" | Out-file -append $outputFile
    "#############################" | Out-file -append $outputFile
    "$Win7 --> Windows-7" | Out-file -append $outputFile
    "$WinXP --> Windows-Xp" | Out-file -append $outputFile
    "$Win8 --> Windows-8" | Out-file -append $outputFile
    "$Win10 --> Windows-10" | Out-file -append $outputFile
    "$Win2000Pro --> Windows-2000-Professional" | Out-file -append $outputFile
    "$WinVista --> Windows-Vista" | Out-file -append $outputFile
    "$Win2000Server --> Windows-Server-2000" | Out-file -append $outputFile
    "$WS2003 --> Windows-Server-2003" | Out-file -append $outputFile
    "$WS2008 --> Windows-Server-2008" | Out-file -append $outputFile
    "$WS2008R2 --> Windows-Server-2008 R2" | Out-file -append $outputFile
    "$WS2012 --> Windows-Server-2012" | Out-file -append $outputFile
    "$WS2012R2 --> Windows-Server-2012 R2" | Out-file -append $outputFile
    "---------------------------------------------" | Out-file -append $outputFile
    "$StudentPC --> Student PCs (total)" | Out-file -append $outputFile
    "$StudentPC_10 --> Student PCs (Windows 10 only)" | Out-file -append $outputFile
    "$WorkPC --> Employee PCs (total)" | Out-file -append $outputFile
    "$WorkPC_10 --> Employee PCs (Windows 10 only)" | Out-file -append $outputFile
    "$Server --> Servers" | Out-file -append $outputFile
    "---------------------------------------------" | Out-file -append $outputFile
    "                                             " | Out-file -append $outputFile
}

"##########################################################" | Out-file -append $outputFile
"##########################################################" | Out-file -append $outputFile
"                                                          " | Out-file -append $outputFile
"#############################" | Out-file -append $outputFile
"##  DOMAIN TOTALS  ##" | Out-file -append $outputFile
"#############################" | Out-file -append $outputFile
"$Win7_total --> Windows-7" | Out-file -append $outputFile
"$WinXP_total --> Windows-Xp" | Out-file -append $outputFile
"$Win8_total --> Windows-8" | Out-file -append $outputFile
"$Win10_total --> Windows-10" | Out-file -append $outputFile
"$Win2000Pro_total --> Windows-2000-Professional" | Out-file -append $outputFile
"$WinVista_total --> Windows-Vista" | Out-file -append $outputFile
"$Win2000Server_total --> Windows-Server-2000" | Out-file -append $outputFile
"$WS2003_total --> Windows-Server-2003" | Out-file -append $outputFile
"$WS2008_total --> Windows-Server-2008" | Out-file -append $outputFile
"$WS2008R2_total --> Windows-Server-2008 R2" | Out-file -append $outputFile
"$WS2012_total --> Windows-Server-2012" | Out-file -append $outputFile
"$WS2012R2_total --> Windows-Server-2012 R2" | Out-file -append $outputFile
"---------------------------------------------" | Out-file -append $outputFile
"$StudentPC_total --> Student PCs (total)" | Out-file -append $outputFile
"$StudentPC_10_total --> Student PCs (Windows 10 only)" | Out-file -append $outputFile
"$WorkPC_total --> Employee PCs (total)" | Out-file -append $outputFile
"$WorkPC_10_total --> Employee PCs (Windows 10 only)" | Out-file -append $outputFile
"$Server_total --> Servers" | Out-file -append $outputFile
"---------------------------------------------" | Out-file -append $outputFile
"                                             " | Out-file -append $outputFile