﻿$doms = (Get-ADForest).Domains

$s = ""

foreach ($d in $doms) {
    Get-ADComputer -Filter { OperatingSystem -like "*Windows Server*200*"} -Properties LastLogonDate, IPv4Address, OperatingSystem -Server $d `
        | select-object -Property Name, DNSHostName, @{Name=”LogonDate”;Expression={$_.LastLogonDate.ToString(“dd-MM-yyyy”)}}, DistinguishedName, IPv4Address, OperatingSystem `
        | export-csv C:\Skripty\__adamove_veci.csv -NoTypeInformation -Append
}

#$s | export-csv C:\Skripty\__adamove_veci.csv -NoTypeInformation

#$s


<#
get-adcomputer -Filter { OperatingSystem -like "*Windows Server*"} -Properties LastLogonDate, IPv4Address, OperatingSystem `
| select-object -Property Name, DNSHostName, @{Name=”LogonDate”;Expression={$_.LastLogonDate.ToString(“dd-MM-yyyy”)}}, DistinguishedName, IPv4Address, OperatingSystem `
| export-csv C:\Skripty\all-servers-$((Get-ADDomain).name)_$((Get-Date).ToString('dd-MM-yyyy')).csv -NoTypeInformation
#>