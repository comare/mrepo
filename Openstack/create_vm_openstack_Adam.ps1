
<#function name_from_id {
    param ($name)

    $Mathces = @{}  # clear out Matches
    $regex = "\| name\s+\| (?<net>[a-z0-9\-]*)"
    $out = openstack subnet show $name
    $a = $out | where {$_ -match $regex}
    return $Matches.net
}#>

function build_command_partial {
    param( $name, $info)

    $split = $info.split('|')

    $ips = $split[0].split(',')
    $nets = $split[1].split(',')
    $img = $split[2]
    $flav = $split[3]

    if ($ips.Count -ne $nets.Count) {
        echo "provide one network for each ip"
        exit
    }

    $cmd = "openstack server create $name"

    for ($i = 0; $i -lt $ips.Count; $i++) {
        $cmd += " --nic net-id=$($nets[$i]),v4-fixed-ip=$($ips[$i])"
    }

    $cmd += " --image $img --flavor $flav "

    return $cmd
}

#statics
$key = "debian"
$adm_pass = "Pa1234ss"
$cert = "..\cert.pem"

#semi-static
$image = "windows-10-amd64-v3"
$flavor = "csirtmu.small2x8"
<#
| allocation_pools     | 192.168.12.226-192.168.12.254        |
| cidr                 | 192.168.12.224/27                    |
| dns_nameservers      | 147.251.4.33, 147.251.6.10, 8.8.8.8  |
| gateway_ip           | 192.168.12.225                       |
| host_routes          |                                      |
| id                   | d758baf8-5115-40cf-a2f1-776cdafb9abf |
#>
$subnet = "windows-bootcamp"
$netid = network_from_subnet $subnet


#dynamic
$ip = "192.168.12.230"
$name = "a_tst"

#servers
$servers = @{
    #name           ip1,ip2 | net1,net2 (network ids) | image | flavor
    #"a_tst"     =   "192.168.12.234,172.18.1.190|8a37fd24-702d-483e-9d55-044109c2200e,def02b26-4f67-4577-a9ff-8ae58cc74085|windows-10-amd64-v3|csirtmu.small2x8";
    "usr-windows-1"     =   "192.168.12.234|8a37fd24-702d-483e-9d55-044109c2200e|windows-10-amd64-v3|csirtmu.tiny1x2";
    "usr-windows-2"     =   "192.168.12.235|8a37fd24-702d-483e-9d55-044109c2200e|windows-10-amd64-v3|csirtmu.tiny1x2";
    "usr-windows-3"     =   "192.168.12.236|8a37fd24-702d-483e-9d55-044109c2200e|windows-10-amd64-v3|csirtmu.tiny1x2";
    "usr-windows-ext"   =   "192.168.14.10|083baf86-ad47-4126-b628-e0688914c220|windows-10-amd64-v3|csirtmu.tiny1x2";
    "windows_tools"     =   "192.168.12.237|8a37fd24-702d-483e-9d55-044109c2200e|windows-server-2019-amd64-v5|csirtmu.tiny1x4";
    "windows_dc"        =   "192.168.12.238|8a37fd24-702d-483e-9d55-044109c2200e|windows-server-2019-amd64-v5|csirtmu.tiny1x4";   
}

foreach ($name in $servers.Keys) {

    $command = build_command_partial $name $servers[$name]

    $command += " --key $key --property admin_pass=$adm_pass --user-data $cert"

    invoke-expression $command
}

<#
#create port group
$portname = "$($name)_port"
openstack port create $portname `
    --network $netid `
    --fixed-ip subnet=$subnet,ip-address=$ip 

#create server
openstack server create $name `
    --port $portname `
    --image $image `
    --flavor $flavor `
    --key $key `
    --property admin_pass=$adm_pass `
    --user-data $cert
#>




