$csv = Import-Csv -Path bankovniky.csv

$servers = [ordered] @{}

foreach ($line in $csv) {

    $template_server = [ordered] @{
        "address"=  $line.address
        "display_name"=  $line.name
        "groups" =  @("windows-servers"; "winadm-ucn")
        "check_command"=  "hostalive"
        "vars"=  @{
            "service_sets" = @("default")
            "notification_sets" = @("default-server")
        }
    }

    $template_service = [ordered] @{
        "display_name"= $line.name
        "check_command"= "nrpe"
        "vars" = @{
            "nrpe_command"=$line.check
            "nrpe_port"="5669"
            "notifications"= @("winadm-service-down-onetime")
        }
        "check_interval"= 600
        "retry_interval"= 60
        "groups"= @("bankovniky")        
    }

    $servers.add($line.check,$template_service)
}

ConvertTo-Json -InputObject $servers -Depth 10 | Out-File New_Config.json

$servers