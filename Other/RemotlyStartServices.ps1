﻿$servers = @(
"phil-server2.phil.ucn.muni.cz"
)

foreach ($server in $servers) {
    Write-Output "Connecting to server $server"
    $services = Get-Service -Name winlogbeat, metricbeat, filebeat -ComputerName $server | Where-Object -Property Status -eq Stopped
    $services | Start-Service
    Write-Output "Services started"
    Get-Service -Name  winlogbeat, metricbeat, filebeat -ComputerName $server
    Write-Output "**********************************************************"
}