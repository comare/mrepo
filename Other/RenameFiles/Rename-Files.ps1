# Select folder to browse
Add-Type -AssemblyName System.Windows.Forms
$FolderBrowser = New-Object System.Windows.Forms.FolderBrowserDialog
[void]$FolderBrowser.ShowDialog()
$folderrootpath = $FolderBrowser.SelectedPath

# List directories with number of files
$directories = Get-ChildItem -Path $folderrootpath -Directory
if (($directories).count -eq 0) {
    $directories = Get-Item -Path $folderrootpath
}

foreach ($directory in $directories) {
    $count = ($directory.GetFiles() | Measure-Object).Count
    Write-Host "Folder:" $directory.Name "- number of files" $count -ForegroundColor Yellow
}
# Manual stop besause of rights on VM. Or find out any check 
Write-Host "Do you want to rename these folder\s?"
$input_key = Read-Host -Prompt 'To continue type "Y"'

if ($input_key -eq "Y") {
    # Renaming files
    foreach ($directory in $directories) {
        Write-Host "Folder:" $directory.Name -ForegroundColor Green
        $files = $directory.GetFiles()
        $count = 1
        foreach ($file in $files) {
            $newname = $directory.Name + "_" + $count.ToString("00000") + $file.Extension
            Write-Host $file.Name "- will be renamed to" $newname
            Rename-Item -Path $file.FullName -NewName $newname
            $count++
        }
        Write-Host "*********************************************"
    }
}
Read-Host -Prompt 'Press any key to exit script'