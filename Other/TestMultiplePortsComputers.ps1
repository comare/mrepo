$ports = @(53,88,135,389,445,636)
$computers = @("vfu-dc.vfu.cz", "dc2.vfu.cz", "dc3.vfu.cz")
foreach ($computer in $computers) {
    foreach ($port in $ports) {
        $test = Test-NetConnection -ComputerName $computer -Port $port -InformationLevel Quiet
        Write-Output ('Has {0} opened port {1}: {2}' -f $computer, $port, $test)
    }
}