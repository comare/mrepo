function Test-Remoting {
    param (
        [Parameter(ParameterSetName='manual')]
        [string[]]$computerName,
        [Parameter(ParameterSetName='csv')]
        [string]$csvfile
    )
    
    if ($PSCmdlet.ParameterSetName -eq 'manual') {
        [string[]]$list = $computerName
    }
    if ($PSCmdlet.ParameterSetName -eq 'csv') {
        [array]$list = @()
        $importedFile = Import-Csv -Path $csvfile
        foreach ($f in $importedFile) {
            $list += $f.ListToImport
        }
    }
    
    Write-Output "Testing Remote connection from $env:COMPUTERNAME"
    foreach ($l in $list) {
        Write-Output "Computer $l"
        Start-Job -ArgumentList $l -ScriptBlock {
            $l = $args[0]
            [bool]$remotingresult = $False
            if (Test-WSMan -ComputerName $l -ErrorAction SilentlyContinue) {
                $remotingresult = $True
            }
            $pingresult = Test-NetConnection -ComputerName $l

            Write-Output "$l $($pingresult.BasicNameResolution.ipaddress) open for PSremoting? $remotingresult and ping? $($pingresult.PingSucceeded)"
        }
    }
    While (Get-Job -State "Running") {    
        Write-Output "Running..."
        Start-Sleep 5
    }
    Get-Job | Receive-Job 
    Get-Job | Remove-Job
}