Write-Host "Enter path to csv file (with Column UCO), separated by ;"
$pathToFile = Read-Host -Prompt "Enter path: "

$users = Import-Csv -Path $pathToFile -Delimiter ";"

$group = 'Zamestnanec'

foreach ($user in $users) {
    $memberofgroups = Get-ADPrincipalGroupMembership -Identity $user."UCO" | Select-Object -ExpandProperty SamAccountName
    If (!($memberofgroups -contains $group)) {
       Write-Host "$($user."UCO") is not a member of $group"
    }
}