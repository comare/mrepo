#Search in NohIDread and ManagedAccount

$users = Get-ADUser -SearchBase "OU=MU,DC=ucn,DC=muni,DC=cz" -Filter "Enabled -eq 'True'"

$securedgroups = @('NohIDread','vfu test','Test-Acc-RDP','LibAll','Zamestnanec','Student','Host','VPN','Obsluha  CPS','ManagedAccount','emer-prof','teamcity')

foreach ($user in $users) {
    $accountCanReadHouseIdentifier = 1
    $memberofgroups = Get-ADPrincipalGroupMembership -Identity $user | Select-Object -ExpandProperty SamAccountName
    foreach ($memberofgroup in $memberofgroups) {
        If (($securedgroups -contains $memberofgroup)) {
            $accountCanReadHouseIdentifier = 0
        }
    }
    if ($accountCanReadHouseIdentifier) {
        $output = "$($user.SamAccountName) - $($user.DistinguishedName) can read ID"
        Write-Output  $output
        $output | Out-File -FilePath .\Users.txt -Append -Force
    }
}
