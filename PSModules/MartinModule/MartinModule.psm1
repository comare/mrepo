﻿$MartinLogPreference = "c:\test\errors.txt"

function Get-OSInfo {
    # This command is there for support -whatif, -confirm and CommonParameters - not needed in this script
    [CmdletBinding(SupportsShouldProcess=$True)]

    param(
        [Parameter(Mandatory=$True,
                  ValueFromPipeline=$True,
                  ValueFromPipelineByPropertyName=$True,
                  HelpMessage="The. Computer. Name.")]
        [Alias('Hostname','cn')]
        [string[]]$computerName,
        
        [Parameter()]
        [string]$ErrorLogFilePath = $MartinLogPreference
    )
    BEGIN{
        Remove-Item -Path $ErrorLogFilePath -Force -ErrorAction SilentlyContinue
        $ErrorsHappened = $False
    }
    PROCESS {
        foreach ($computer in $computerName) {
            try {
                $session = New-CimSession -ComputerName $computer -ErrorAction Stop
                $os = Get-CimInstance -ClassName win32_operatingsystem -CimSession $session
                $cs = Get-CimInstance -ClassName win32_computersystem -CimSession $session
                $properties = @{ComputerName = $computer
                                Status = 'Connected'
                                SPVersion = $os.ServicePackMajorVersion
                                OSVersion = $os.Version
                                Model = $cs.Model
                                Mfgr = $cs.Manufacturer}
            } catch {
                Write-Verbose "Couldn't connect to $computer"
                $computer | Out-File $ErrorLogFilePath -Append
                $ErrorsHappened = $True
                $properties = @{ComputerName = $computer
                                Status = 'Disconnected'
                                SPVersion = $null
                                OSVersion = $null
                                Model = $null
                                Mfgr = $null}
            } finally {
                $obj = New-Object -TypeName PSObject -Property $properties
                Write-Output $obj    
            }
        }
    }
    END{
        if ($ErrorsHappened) {
            Write-Warning "Errors logged to $ErrorLogFilePath"
        }
    }
}