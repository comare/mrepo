<#
.Synopsis
    Function for joining domain and moving server to correct OU
.DESCRIPTION
    Function for joining domain and moving server to correct OU
.NOTES
    Version:        0.1 Beta
    Author:         Martin Kotlik
    Creation Date:  28.09.2019
    Purpose/Change: 0.1 Modul was created
.EXAMPLE
    Add-WinServerToDomain -domain dis.ics.muni.cz
    Add-WinServerToDomain -domain dis.ics.muni.cz -OU 'OU=MakariosOU,OU=gisOU,OU=Servers_New,DC=dis,DC=ics,DC=muni,DC=cz'
#>

function Add-WinServerToDomain {
    [CmdletBinding()]
    param (
        # domain in which you want to add server
        [Parameter(Mandatory = $true)]
        [ValidateSet(,'ucn.muni.cz','dis.ics.muni.cz')]
        [string] $domain
        ,
        # domain in which you want to add server - if empty the default OU is selected by domain
        [string] $OU
    )

    #Set path for server in AD - default for each domain or custom if inserted
    if ([string]::IsNullOrEmpty($OU)) {
        if ($domain -eq 'dis.ics.muni.cz') {
            $OU = 'OU=Servers_New,DC=dis,DC=ics,DC=muni,DC=cz'
        }
        if ($domain -eq 'ucn.muni.cz') {
            $OU = 'OU=Incoming,DC=ucn,DC=muni,DC=cz'
        }
    }

    #Adding server to domain
    $admin_cred = Get-Credential  -Message "Enter domain admin credentials for adding server to domain:"
    Add-Computer -DomainName $domain -Credential $admin_cred -OUPath $OU -restart
}