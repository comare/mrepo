<#
.Synopsis
    Function moving server to OU
.DESCRIPTION
    Function moving server to OU
    If OU does not exist it will be created
.NOTES
    Version:        0.1 Beta
    Author:         Martin Kotlik
    Creation Date:  28.09.2019
    Purpose/Change: 0.1 Modul was created
.EXAMPLE
    Move-WinServerToOU -server makarios -OU 'OU=MakariosOU,OU=gisOU,OU=Servers_New,DC=dis,DC=ics,DC=muni,DC=cz'
    Move-WinServerToOU -server ucn09 -OU 'OU=UCN09TEST,OU=TestOU,OU=Servers_SCCM,DC=ucn,DC=muni,DC=cz'
#>

#TODO add try catch blocks
function Add-NewPathToOU {
    param (
        [string]$Path
    )
    [string[]]$splitOU = $ou.Split(',')
    [string[]]$OUPaths = @()

    for ($i=0;$i -le $splitOU.Length-1;$i++) {
        $OUPaths += $splitOU[$i..$splitOU.Length] -join ","
    }

    for ($i=$OUPaths.Length-1; $i -ge 0; $i--) {
        [string]$path = $OUPaths[$i]
        if (($path[0,1] -join "") -eq "OU") {
            if (-not ([adsi]::Exists("LDAP://$path"))) {
                [string[]]$NewOUSplit = (($path.Remove(0,3)).Split(","))
                [string]$NewOU = $NewOUSplit[0]
                [string]$NewOUPath = $NewOUSplit[1..$NewOUSplit.Length] -join ","
                New-ADOrganizationalUnit -Name $NewOU -Path $NewOUPath
            }
        }
    }
}
function Move-WinServerToOU {
    [CmdletBinding()]
    param (
        # add or remove disk parametr
        [Parameter(Mandatory = $true)]
        [string] $server
        ,
        # virtual machine in which disk will be added or removed
        [Parameter(Mandatory = $true)]
        [string] $OU
    )

    $serverObject = Get-ADObject -Filter 'Name -eq $server -and ObjectClass -eq "computer"'

    if ([adsi]::Exists("LDAP://$OU")) {
        Move-ADObject -Identity $serverObject.DistinguishedName -TargetPath $OU
    } else {
        Add-NewPathToOU -Path $OU
        Move-ADObject -Identity $serverObject.DistinguishedName -TargetPath $OU
    }

   
}