<#
.Synopsis
   Function for configuration of network and computername
.DESCRIPTION
   Function for configuration of network and computername.
   Script is prepared for one network adapter and there is a check on begining.
.NOTES
   Version:        0.1 Beta
   Author:         Martin Kotlik
   Creation Date:  28.09.2019
   Purpose/Change: 0.1 Modul was created
.EXAMPLE
   Set-WinServerNetwork -IP 192.168.2.1 -Prefix 24 -DNS 192.168.3.1,192.168.3.2 -Gateway 192.168.2.254 -NewServerName Server-app01
#>

function Set-WinServerNetwork {
    [CmdletBinding()]
    Param
    (
        # IP address of VM adapter
        [Parameter(Mandatory = $true)]
        [string] $IP
        ,
        # Prefix number for VM adapter
        [Parameter(Mandatory = $true)]
        [byte] $Prefix
        ,
        # List of DNS adresses to be set for VM (2 is most common)
        [Parameter(Mandatory = $true)]
        [string[]] $DNS
        ,
        # Gateway address for VM adapter
        [Parameter(Mandatory = $true)]
        [string] $Gateway
        ,
        # New name for server
        [Parameter(Mandatory = $true)]
        [string] $NewServerName
    )

    # Disable IPV6 Binding on Ethernet adapter - If there is 0 or more than 1 adapters the error is written
    $adapter = Get-NetAdapter
    
    if (($adapter | Measure-Object).count -ne 1) {
        Write-Error "Computer has 0 or more than 1 ethernet adapters - need manual configuration"
    } else {
        Disable-NetAdapterBinding -Name $adapter.Name -ComponentID ms_tcpip6 -PassThru

        Remove-NetIPAddress -InterfaceIndex $adapter.InterfaceIndex
        New-NetIPAddress -IPAddress $IP -DefaultGateway $Gateway -PrefixLength $Prefix -AddressFamily IPv4 -InterfaceIndex $adapter.InterfaceIndex -Confirm:$false

        Set-DNSClientServerAddress -InterfaceIndex $adapter.InterfaceIndex -ServerAddresses $DNS
    }

    if ($env:COMPUTERNAME -ne $NewServerName) {
        Rename-Computer -NewName $NewServerName -Restart        
    }
}