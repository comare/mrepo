﻿<#
.Synopsis
   Function for connecting to vmware
.DESCRIPTION
   Function for connecting to vmware. Tested on module PowerCLI 10.2.0.9372002. Modul must be imported.
.NOTES
   Version:        0.1
   Author:         Ondrej Buday
   Creation Date:  6.8.2019
   Purpose/Change: 0.1 Modul was created

.EXAMPLE
    Connect-VIServer   
.EXAMPLE
    Connect-VIServer -Server vcenter.ics.muni.cz   
#>

function Connect-Vcenter
{
    [CmdletBinding()] # 
    Param
    (
        # Set vmware center, default is vc.ps.dis.ics.muni.cz
        [Parameter(
            Mandatory = $false,
            Position = 0
        )]
        [ValidateSet('vc.ps.dis.ics.muni.cz','vcenter.ics.muni.cz')]
        [Alias('vc')]
        $VcenterName = 'vc.ps.dis.ics.muni.cz'
    )

    # ICS Credentials
    $cred = Get-Credential
    
    # Connect to vmware center 
    Connect-VIServer -Server $VcenterName -Protocol https -Credential $cred
}