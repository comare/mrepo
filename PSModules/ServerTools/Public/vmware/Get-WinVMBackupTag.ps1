﻿<#
.Synopsis
   Function for list Backup Tag for VM/VMs in HA VMware.
.DESCRIPTION
   Function for list Backup Tag for VM/VMs in HA VMware.
.NOTES
   Version:        0.1
   Author:         Ondrej Buday
   Creation Date:  9.9.2019
   Purpose/Change: 0.1 Modul was created
.EXAMPLE
   Get-WinVMBackubTag
.EXAMPLE
   Get-WinVMBackubTag -VM server1
.EXAMPLE
   Get-WinVMBackubTag -VM server1, server2, server3
.EXAMPLE
   Get-WinVMBackubTag -NoBackupTag
#>

function Get-WinVMBackubTag {
    [CmdletBinding()]
    Param
    (
        <# Set VM/VMs Name - Multimple values are possible
           Command with empty VM parameter show you All Windows backup tags configured to VMs          
        #>
        [Parameter(
            Mandatory = $false
        )]
        [Alias('vmname')]
        [string[]]
        $VM = '*'
        ,
        <# List only VMs without Backup tag
        #>
        [Parameter(
            Mandatory = $false
        )]
        [switch]
        $NoBackupTag
    )

    $WinVMs = Get-VM $VM

    # Initialize an array
    $WinVMbackupTags = @()
    
    foreach ($WinVM in $WinVMs) {     
        $Tag = Get-TagAssignment -Entity $WinVM -Category 'Backup Scenario'
        # Create a PSObject with the associated properties hashtable and add to $WinVMbackupTags
        if ($Tag) {
            $WinVMbackupTags += new-object PSObject -Property @{VM = $WinVM.Name; BackupTag = $Tag.Tag.Name; Description = $Tag.Tag.Description}
            #Write-Host $WinVM.Name $Tag.Tag.Name $Tag.Tag.Description
        }
        else {
            $WinVMbackupTags += new-object PSObject -Property @{VM = $WinVM.Name; BackupTag = ""; Description = "There is no backup"}
            #Write-Host $WinVM.Name "No backup"
        }
    }

    if ($NoBackupTag.IsPresent) {
        $WinVMbackupTags | Where-Object -Property Description -Like "There is no backup" | Select-Object -Property VM, Backuptag, Description | Sort-Object -Property VM
    }
    else {
        $WinVMbackupTags | Select-Object -Property VM, Backuptag, Description | Sort-Object -Property VM
    } 

}


