﻿<#
.Synopsis
   Function to mount installation ISO to VM.
.DESCRIPTION
   Function to mount installation ISO to VM. You can start VM when ISO is mounted.
.NOTES
   Version:        0.1
   Author:         Ondrej Buday
   Creation Date:  8.8.2019
   Purpose/Change: Function to mount installation ISO to VM.
   Changelog:   08.10.2019 - 0.2 Kotlik Martin - Added iso value for DIS domain
.EXAMPLE
   Mount-WinISOtoVM -VMName server1
.EXAMPLE
   Mount-WinISOtoVM -VMName server1 -StartVM
.EXAMPLE
   Mount-WinISOtoVM -VMName server1 -Datastore hdd-images -ISOPath win2022.iso -StartVM
#>

function Mount-WinISOtoVM
{
    [CmdletBinding()]
    Param
    (
        <# Set VM Name.
        #>
        [Parameter(
            Mandatory = $true,
            Position = 0
        )]
        [Alias('vm')]
        [string]
        $VMName
        ,
        <# Set ISO's datastore, default datastore is dx500cps-NLSAS-l1-images.
        List datastores with command Get-Datastore. #>
        [Parameter(
            Mandatory = $false,
            Position = 1
        )]
        [Alias('store')]
        [ValidateSet('dx500cps-NLSAS-l1-images','storw-p2-l1-r5-images')]
        [string]
        $Datastore = 'dx500cps-NLSAS-l1-images'
        ,
        <# Set ISO path, default path is w2019svr_std_dc_en_64b_unattend_v2019-08-07.iso.
        #>
        [Parameter(
            Mandatory = $false,
            Position = 2
        )]
        [Alias('iso')]
        [string]
        $ISOPath = 'w2019svr_std_dc_en_64b_unattend_v2019-08-07.iso'
        ,
        <# Set -StartVM parameter if VM should be run after create
        #>
        [Parameter(
            Mandatory = $false,
            Position = 3
        )]
        [Alias('start')]
        [switch]
        $StartVM
    )

    $ErrorActionPreference = "Stop"
    
    # Check if CDDrive is present, if not assign new CDDrive to VM
    if (!(get-vm $VMName | Get-CDDrive).Id)
    {
        Get-VM $VMName | New-CDDrive
    }
    
    # Mount ISO to VM
    #Get-VM $VMName | Get-CDDrive | Set-CDDrive -IsoPath "[$Datastore] $ISOPath" -StartConnected:$true -Confirm:$false
    Get-VM $VMName | Get-CDDrive | Set-CDDrive -IsoPath ('[{0}] {1}' -f $Datastore, $ISOPath) -StartConnected:$true -Confirm:$false 

    if ($StartVM.IsPresent)
    {
        # Run VM
        Start-Sleep 5
        Get-VM -Name $VMName | Start-VM
    }
}