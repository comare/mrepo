

function New-OsDeploy {

    Param(
        <# Set New VM Name. Please use small letters.
        #>
        [Parameter(Mandatory = $true)]
        [string]$Name,
        [Parameter(Mandatory=$true)]
        [string]$TemplateName,
        [Parameter(Mandatory=$true)]
        [string]$AdminPass,
        [pscredential]$DomainCred = $null,
        [string]$Domain = 'ucn.muni.cz',
        [Parameter(Mandatory=$true)]
        [string]$IpAddress,
        [Parameter(Mandatory=$true)]
        [string]$SubnetMask,
        [Parameter(Mandatory=$true)]
        [string]$DefaultGateway,
        [Parameter(Mandatory=$true)]
        [string]$Dns1,
        [Parameter(Mandatory=$true)]
        [string]$Dns2,
        [string]$Organization = 'MasarykUniversity',
        [string]$OwnerFullName = 'Intitute of Computer Science',
        <# see https://www.vmware.com/support/developer/windowstoolkit/wintk40u1/html/New-OSCustomizationSpec.html
        #>
        [string]$TimeZoneCode = '095'
    )

    $ErrorActionPreference = "Stop"

    $spec
    try {
        $spec = New-OSCustomizationSpec `
                -Name "$Name-tempSpec" `
                -OSType windows `
                -ChangeSid `
                -FullName $OwnerFullName `
                -OrgName $Organization `
                -TimeZone $TimeZoneCode `
                -Domain $Domain `
                -DomainCredentials $DomainCred `
                -AdminPassword $AdminPass `
                -NamingScheme fixed `
                -NamingPrefix $Name
    } catch {
        Write-Host "Error while creating new specification: " -ForegroundColor Red
        $_
        Remove-OSCustomizationSpec $spec -Confirm:$false
        return
    }
    
    try {
        Get-OSCustomizationNicMapping -OSCustomizationSpec $spec | `
            Set-OSCustomizationNicMapping `
                -IpMode UseStaticIP `
                -IpAddress $IpAddress `
                -SubnetMask $SubnetMask `
                -DefaultGateway $DefaultGateway `
                -Dns $Dns1, $Dns2
    } catch {
        Write-Host "Error while setting up network: " -ForegroundColor Red
        $_
        Remove-OSCustomizationSpec $spec -Confirm:$false
        return
    }
}