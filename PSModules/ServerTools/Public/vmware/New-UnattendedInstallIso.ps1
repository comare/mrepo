<#
	.NOTES
		Author  : Adam Dobias

		Version : 1.0
		Created : 2019/08/01

	.DESCRIPTION 
        Add VMWare tools and Autounattend.xml file to selected ISO image.
        The tools will be silently installed after Windows installation (by SetupComplete.cmd script).
        Works with ESXI 5.1 or later (edit required for 5.0.x, see relevant section of script).

        Requires win ADK.

	.EXAMPLE
        New-UnattendedInstallIso -isoPath C:\template.iso -output C:\autoinstall.iso -Pass 132456798 -UnattendTemplate C:\template.xml -ImageIndex 2
        Will add WMWare Tools, silent install script and Autounattend.xml answer file to the template.iso and save it as autoinstall.iso.
        Image with index 2 will be installed.
        Deletes temp folder on both error and success. 

	.EXAMPLE
        New-UnattendedInstallIso -isoPath C:\template.iso -output C:\autoinstall.iso -Pass 132456798 -UnattendTemplate C:\template.xml
        WWill add WMWare Tools, silent install script and Autounattend.xml answer file to the template.iso and save it as autoinstall.iso.
        The user will be asked to select which image should be installed.
        Deletes temp folder on both error and success.

	.PARAMETER IsoPath	
        Path to the iso image
        
    .PARAMETER Output
        Path to the newly created iso.

    .PARAMETER UnattendTemplate
        Path to .xml file which should be used as a template for unattend.xml

    .PARAMETER Pass
        Pass to be set as temporary local admin password.

    .PARAMETER ImageIndex
        Index of Image which will be installed.
        The script will ask for one if none is provided.

    .Parameter Cleanup
        If temp files hsould be deleted after script executed or encountered error.
        Default is True.

#>

function New-UnattendedInstallIso {
    Param (
        [Parameter(Mandatory=$true)]
        [string]$IsoPath,
        [Parameter(Mandatory=$true)]
        [string]$Output,
        [Parameter(Mandatory=$true)]
        [string]$UnattendTemplate,
        [Parameter(Mandatory=$true)]
        [string]$Pass,
        [int]$ImageIndex = 0,
        [boolean]$Cleanup = $true
    )

    $ErrorActionPreference = "Stop"

    if (!(Test-Path $IsoPath)) {
        Write-Error "Cannot reach the iso file. Perhaps it does not exist."
    }

    if (!(Test-Path $UnattendTemplate)) {
        Write-Error "Cannot reach the template file. Perhaps it does not exist."
    }

    $temp = "C:\temp"
    $root = "$temp\New-UnattendedInstallIso_Temp"
    $drivers = "$root\Drivers"
    $winIso = "$root\ISO"
    $mount = "$root\Mount"
    $vmwareTemp = "$root\VMWareTemp"
    if (!(Test-path $temp)) {
        mkdir $temp
    }

    try {
        mkdir $root, $drivers, $winIso, $mount, $vmwareTemp
    } catch {
        Write-Host "Error while creating temporary files: " -ForegroundColor Red
        $_
        if ($Cleanup) {
            "Cleaning up..."
            Remove-Item -Force -Recurse $root -ErrorAction Continue 
        }
        return
    }
    
    # Download WMWare Tools ISO
    $rootUrl = 'https://packages.vmware.com/tools/esx/latest/windows/'
    $name = ""
    try {
        Write-Host "Downloading WMW tools iso"
        $web = Invoke-WebRequest -Uri $rootUrl
        foreach ($link in $web.Links) {
            if ($link.href -like '*.iso') {
                $name = $link.href
            }
        }
        if ([string]::IsNullOrWhiteSpace($name)) {
            Write-Error "No .iso file found."
        }
        $wc = New-Object System.Net.WebClient
        $wc.DownloadFile($rootUrl + $name, "$drivers\$name")
    } catch {
        Write-Host "Encountered error while trying to download the file: " -ForegroundColor Red
        $_
        if ($Cleanup) {
            "Cleaning up..."
            Remove-Item -Force -Recurse $root -ErrorAction Continue 
        }
        return
    }
    
    # mount VMWare Tools ISO
    try {
        Write-Host 'Mounting WMW tools iso'
        $vol = Mount-DiskImage -ImagePath "$drivers\$name" -PassThru | Get-DiskImage | Get-Volume 
        Write-Host 'Copying WMW tools iso contents'
        Copy-Item "$($vol.DriveLetter):\*" $vmwareTemp -Recurse
    } catch {
        Write-Host "Error extracting files from VMWare ISO: " -ForegroundColor Red
        $_
        if ($Cleanup) {
            "Dismounting iso..."
            Dismount-DiskImage -ImagePath "$drivers\$name"
            "Cleaning up..."
            Remove-Item -Force -Recurse $root -ErrorAction Continue 
        }
        return
    }
    
    # Mount Windows ISO
    try {
        Write-Host 'Mounting Windows iso'
        $vol = Mount-DiskImage -ImagePath $IsoPath -PassThru | Get-DiskImage | Get-Volume
        Write-Host 'Copying Windows iso contents'
        Copy-Item "$($vol.DriveLetter):\*" $winIso -Recurse
    } catch {
        Write-Host "Error extracting files from windows ISO: " -ForegroundColor Red
        $_
        if ($Cleanup) {
            "Dismounting VMWTools iso..."
            Dismount-DiskImage -ImagePath "$drivers\$name"
            "Dismounting iso..."
            Dismount-DiskImage -ImagePath $IsoPath -ErrorAction Continue            
            "Cleaning up..."
            Remove-Item -Force -Recurse $root -ErrorAction Continue 
        }
        return
    }    
    try {
        Write-Host 'Editting XML'
        [xml]$doc = Get-Content -Path $UnattendTemplate
        $nsMgr = New-Object System.Xml.XmlNamespaceManager($doc.NameTable)
        $nsMgr.AddNamespace("ns", $doc.DocumentElement.NamespaceURI)
        $pwds = $doc.SelectNodes("//ns:Password", $nsMgr)
        $pwds | ForEach-Object {
            $_.Value = $Pass
            #$_.InnerText = $Pass
        }
        $ii = $doc.SelectNodes("//ns:ImageInstall", $nsMgr)
        $ii.OSImage.InstallFrom.MetaData.Value = "$ImageIndex"
        $doc.Save("$winIso\Autounattend.xml")
    } catch {
        Write-Host "Error creating Autounattend.xml file: " -ForegroundColor Red
        $_
        if ($Cleanup) {
            "Dismounting VMWTools iso..."
            Dismount-DiskImage -ImagePath "$drivers\$name"
            "Dismounting iso..."
            Dismount-DiskImage -ImagePath $IsoPath -ErrorAction Continue            
            "Cleaning up..."
            Remove-Item -Force -Recurse $root -ErrorAction Continue 
        }
        return
    }
    

    # Edit the images
    $images = Get-WindowsImage -ImagePath "$winIso\sources\install.wim"
    $images
    $ok = $true
    while ( !(($ImageIndex -gt 0) -and ($ImageIndex -le $images.Count) -and $ok) ) {
        try {
            
            [int]$ImageIndex = Read-Host "Please select which image will be installed"
        } catch {$ok = $False}
    }

    Set-ItemProperty -Path "$winIso\sources\install.wim" -Name IsReadOnly -Value $false

    try {
        Write-Host 'Mounting Windows image'
        Mount-WindowsImage -Path $mount -ImagePath "$winIso\sources\install.wim" -Index $ImageIndex
    } catch {
        Write-Host "Error extracting files from windows ISO: " -ForegroundColor Red
        $_
        if ($Cleanup) {
            "Dismounting VMWTools iso..."
            Dismount-DiskImage -ImagePath "$drivers\$name"
            "Dismounting iso..."
            Dismount-DiskImage -ImagePath $IsoPath -ErrorAction Continue            
            "Cleaning up..."
            Remove-Item -Force -Recurse $root -ErrorAction Continue 
        }
        return
    }

    try {
        Write-Host 'Copying VMWtools files to image'
        mkdir "$mount\Windows\Setup\Scripts"
        Copy-Item -Recurse $vmwareTemp $mount
        Write-Host 'Creating setupComplete.cmd'
        # REBOOT=ReallySuppress for ESXI 5.0.x and lower (NOT TESTED THO)
        New-Item -Path "$mount\Windows\Setup\Scripts\SetupComplete.cmd" -ItemType File
        Add-Content "$mount\Windows\Setup\Scripts\SetupComplete.cmd" 'C:\VMWareTemp\setup64.exe /S /v "/qn REBOOT=R ADDLOCAL=ALL"'
        Add-Content "$mount\Windows\Setup\Scripts\SetupComplete.cmd" 'rmdir C:\VMWareTemp /s /q'
        Add-Content "$mount\Windows\Setup\Scripts\SetupComplete.cmd" 'rmdir C:\Windows\Setup\Scripts /s /q'

        Dismount-WindowsImage -Path $mount -Save 
    } catch {
        Write-Host "Failed to put script, VMWTools and unattend.xml to the mounted image." -ForegroundColor Red
        $_
        if ($Cleanup) {
            "Dismopunting image..."
            Dismount-WindowsImage -Path $mount -Save -ErrorAction Continue
            "Dismounting VMWTools iso..."
            Dismount-DiskImage -ImagePath "$drivers\$name"
            "Dismounting iso..."
            Dismount-DiskImage -ImagePath $IsoPath -ErrorAction Continue            
            "Cleaning up..."
            Remove-Item -Force -Recurse $root -ErrorAction Continue 
        }
        return
    }

    # Pack it all up into iso
    $oscdimgPth = 'C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Deployment Tools\amd64\Oscdimg\oscdimg.exe'
    #& $oscdimgPth -n -m -b"$winIso\boot\etfsboot.com" $winIso $Output
    & $oscdimgPth -bootdata:2#p0,e,b"$winIso\boot\etfsboot.com"#pEF,e,b"$winIso\efi\microsoft\boot\Efisys.bin" -u1 -udfver102 $winiso $Output
 
    ################
    ## CLEANUP
    ################
    Dismount-DiskImage -ImagePath $IsoPath
    Dismount-DiskImage -ImagePath "$drivers\$name"
    Remove-Item -Force -Recurse $root -ErrorAction Continue 
}
