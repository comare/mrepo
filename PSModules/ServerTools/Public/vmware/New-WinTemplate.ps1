<#
    .Synopsis
    Function for create Windows template in HA VMware.
    .DESCRIPTION
    unction for create Windows template in HA VMware 
    You need installed PowerCLI modul v10
    .NOTES
    Version:        1.0
    Author:         Adam Dobias
    Creation Date:  16.09.2019
    Purpose/Change: unction for create Windows template in HA VMware
    Changelog:
    .EXAMPLE
    New-WinVM -Name server1 -NetworkName Seg12-26-30
    .EXAMPLE
    New-WinVM -Name server1 -NetworkName Seg12-26-30 -VCConnect
    .EXAMPLE
    New-WinVM -Name server1 -NetworkName Seg12-26-30 -NumCpu 8 -MemoryGB 16 -DiskGB 200
    .EXAMPLE
        New-WinVM -Name server1 -TemplateName 'template01' -AdminPass Pa1234ss -IpAddress 147.251.71.210 -SubnetMask 255.255.255.192 -DefaultGateway 147.251.71.193 -Dns1 147.251.16.76 -Dns2 147.251.16.78 -StartVM

    .PARAMETER Vcenter
        Set Vcenter where you wan't create New VM, default is vc.ps.dis.ics.muni.cz.           
        
    .PARAMETER Name
        Set New VM Name. Please use small letters.

    .PARAMETER Datastore
        Set datastore to store New VM, default datastore is sc3-l8-SSD-HA-oss-win.
        List datastores with command Get-Datastore.
    
    .PARAMETER Location
        Set location (folder) to store New VM, default location is Incoming.
        List locations with command Get-Folder.

    .PARAMETER ResourcePool
        Set resource pool, default resource pool is oss-win.
        List resource pools with command Get-ResourcePool.

    .PARAMETER NetworkName
        Set network name.
        We have not permission to list Networks at this moment.
        In PowerCLI v11 this parameter is deprecated. New parameter is -Portgroup but we have not permission to work with this parameter. Use PowerCLI v10 instead and use parameter -NetworkName.
        
        Possible values:
        -- Altariel-internal 
        -- Seg12-128-191 ... 147.251.12.128/26 UVT, UCN, DMZ Vyvojarske univerzitni sluzby, vyvojari OVSS
        -- Seg12-192-255 ... 147.251.12.192/26 UVT, UCN, DMZ Univerzitni sluzby
        -- Seg12-26-30 ... 147.251.12.16/28 UVT, UCN, UPS servery
        -- Seg12-32-63 ... 147.251.12.32/27 UVT, UCN, backend servery ostatni, hypervizory
        -- Seg12-80-95 ... 147.251.12.80/28 UVT, UCN, DMZ spinavy segment, terminalove servery
        -- Seg12-96-126 ... 147.251.12.96/27 UVT, UCN, DMZ Vyvojarske servery
        -- Seg16-132-159 ... 147.251.16.128/27 UVT; OVSS servery
        -- Seg16-64-95 ... 147.251.16.64/27 UVT, UCN, UCN servery v CPS
        -- Seg19-0-63 ... 147.251.19.0/26 UVT, GIS
        -- Seg19-68-94 ... 147.251.19.64/27 UVT, OVSS
        -- Seg19-97-111 ... 147.251.19.96/28 UVT, OSS
        -- Seg63-64-79 ... 147.251.63.64/28 UVT; UCN, STAFF servery
        -- Seg71-160-191-identity ... 147.251.71.160/27 siet pre identitne AD - identities.muni.cz
        -- Seg71-68-126+ipv6 ... 147.251.71.64/26 OSS, servery pro SafeQ5
    
    .PARAMETER NumCpu
        Set Nubmer of vCPU, default value is 4 vCPU.

    .PARAMETER MemoryGB
        Set RAM in GB, default value is 8 GB.

    .PARAMETER DiskGB
        Set HDD size in GB, default values is 100 GB.

    .PARAMETER DiskStorageFormat
        Set Disk Storage Format, default value is Thin. Please leave it default. 

    .PARAMETER GuestId
        Set Operating System Type. Default value is windows9Server64Guest, this is for Windows Server 2016 and newer.
        You can list all possible values with [VMware.Vim.VirtualMachineGuestOsIdentifier].GetEnumValues().
    
    .PARAMETER VCconnect
        Set -VCconnect parameter if you need connect to Vcenter.

    .PARAMETER ISOPath
        Set ISO path, default path is w2019svr_std_dc_en_64b_unattend_v2019-08-07.iso. 

#>

function New-WinTemplate
{
    [CmdletBinding()]
    Param
    (

        [string]$Vcenter = 'vc.ps.dis.ics.muni.cz',            
        [Parameter(
            Mandatory = $true
        )]
        [string]$Name,            

        [string]$Datastore = 'sc3-l8-SSD-HA-oss-win',
        [string]$Location = 'Incoming',
        [string]$ResourcePool = 'oss-win',
        [Parameter(
            Mandatory = $true
        )]
        [string]$NetworkName,
        [int]$NumCpu = '4',
        [int]$MemoryGB = '8',
        [int]$DiskGB = '100',
        [string]$DiskStorageFormat = 'Thin',
        [string]$GuestId = 'windows9Server64Guest',
        [switch]$VCconnect,

        [string]$ISOPath = 'w2019svr_std_dc_en_64b_unattend_v2019-08-07.iso', 
        [string]$IsoDatastore = 'dx500cps-NLSAS-l1-images'

        #[switch]$StartVM
    )

    . .\New-WinVM.ps1
    . .\Mount-WinIsoToVM.ps1
    
    $ErrorActionPreference = "Stop"

    if ($VCconnect.IsPresent)
    {
        # Connect to Vcenter, fill credentials
        Connect-VIServer -Server $Vcenter -Protocol https    
    }

    New-WinVM -Name $Name `
                -Datastore $Datastore `
                -Location $Location `
                -ResourcePool $ResourcePool `
                -NetworkName $NetworkName `
                -NumCpu $NumCpu `
                -MemoryGB $MemoryGB `
                -DiskGB $DiskGB `
                -DiskStorageFormat $DiskStorageFormat `
                -GuestId $GuestId    

    #$vm | New-CDDrive | Set-CDDrive -IsoPath ('[{0}] {1}' -f $Datastore, $ISOPath) -StartConnected:$true -Confirm:$false
    Mount-WinIsoToVM -VMName $Name -Datastore $IsoDatastore -IsoPath $ISOPath -StartVM 

    $sec = 25 * 60
    $timeout = 30
    while ($sec -gt 0) {
        "converting to template in $sec"
        if ($sec -le 60) {
            $timeout = 1
        }
        $sec -= $timeout
        sleep $timeout
    }

    Get-VM -Name $Name | Stop-VM -Confirm:$false | Set-VM -ToTemplate -Confirm:$false
}
