﻿<#
.Synopsis
   Function for create New VM in HA VMware.
.DESCRIPTION
   Function for create New VM in HA VMware. It is designed for UCN VM's. You need installed PowerCLI modul v10
   Requires rights to create OS customization specs in VMware to use with templates.
.NOTES
   Version:        1.0
   Author:         Ondrej Buday
   Creation Date:  8.8.2019
   Purpose/Change: Function for create New VM in HA VMware.
   Changelog:   20.8.2019 - 1.0 Adam Dobias
                                -> added Parameter sets
                                -> added support for creation from template using OS Customization specs 
                                    (including Network)
                                -> updated description and examples
.EXAMPLE
   New-WinVM -Name server1 -NetworkName Seg12-26-30
.EXAMPLE
   New-WinVM -Name server1 -NetworkName Seg12-26-30 -VCConnect
.EXAMPLE
   New-WinVM -Name server1 -NetworkName Seg12-26-30 -NumCpu 8 -MemoryGB 16 -DiskGB 200
.EXAMPLE
    New-WinVM -Name server1 -TemplateName 'template01' -AdminPass Pa1234ss -IpAddress 147.251.71.210 -SubnetMask 255.255.255.192 -DefaultGateway 147.251.71.193 -Dns1 147.251.16.76 -Dns2 147.251.16.78 -StartVM
#>

function New-WinVM
{
    [CmdletBinding()]
    Param
    (
        <# Set Vcenter where you wan't create New VM, default is vc.ps.dis.ics.muni.cz.
        #>
        [Parameter(
            Mandatory = $false,
            Position = 0
        )]
        [Alias('vc')]
        [string]
        $Vcenter = 'vc.ps.dis.ics.muni.cz'
        ,            
        <# Set New VM Name. Please use small letters.
        #>
        [Parameter(
            Mandatory = $true,
            Position = 1
        )]
        [Alias('vmname')]
        [string]
        $Name
        ,            
        <# Set datastore to store New VM, default datastore is sc3-l8-SSD-HA-oss-win.
        List datastores with command Get-Datastore. #>
        [Parameter(
            Mandatory = $false,
            Position = 2
        )]
        [Alias('store')]
        [string]
        $Datastore = 'sc3-l8-SSD-HA-oss-win'
        ,
        <# Set location (folder) to store New VM, default location is Incoming.
        List locations with command Get-Folder. #>
        [Parameter(
            Mandatory = $false,
            Position = 3
        )]
        [Alias('folder')]
        [string]
        $Location = 'Incoming'
        ,
        <# Set resource pool, default resource pool is oss-win.
        List resource pools with command Get-ResourcePool. #>
        [Parameter(
            Mandatory = $false,
            Position = 4
        )]
        [Alias('resource')]
        [string]
        $ResourcePool = 'oss-win'
        ,
        <# Set network name.
        We have not permission to list Networks at this moment.
        In PowerCLI v11 this parameter is deprecated. New parameter is -Portgroup but we have not permission to work with this parameter. Use PowerCLI v10 instead and use parameter -NetworkName.
        
        Possible values:
        -- Altariel-internal 
        -- Seg12-128-191 ... 147.251.12.128/26 UVT, UCN, DMZ Vyvojarske univerzitni sluzby, vyvojari OVSS
        -- Seg12-192-255 ... 147.251.12.192/26 UVT, UCN, DMZ Univerzitni sluzby
        -- Seg12-26-30 ... 147.251.12.16/28 UVT, UCN, UPS servery
        -- Seg12-32-63 ... 147.251.12.32/27 UVT, UCN, backend servery ostatni, hypervizory
        -- Seg12-80-95 ... 147.251.12.80/28 UVT, UCN, DMZ spinavy segment, terminalove servery
        -- Seg12-96-126 ... 147.251.12.96/27 UVT, UCN, DMZ Vyvojarske servery
        -- Seg16-132-159 ... 147.251.16.128/27 UVT; OVSS servery
        -- Seg16-64-95 ... 147.251.16.64/27 UVT, UCN, UCN servery v CPS
        -- Seg19-0-63 ... 147.251.19.0/26 UVT, GIS
        -- Seg19-68-94 ... 147.251.19.64/27 UVT, OVSS
        -- Seg19-97-111 ... 147.251.19.96/28 UVT, OSS
        -- Seg63-64-79 ... 147.251.63.64/28 UVT; UCN, STAFF servery
        -- Seg71-160-191-identity ... 147.251.71.160/27 siet pre identitne AD - identities.muni.cz
        -- Seg71-68-126+ipv6 ... 147.251.71.64/26 OSS, servery pro SafeQ5
        #>
        [Parameter(
            Mandatory = $true,
            Position = 5,
            ParameterSetName = 'Non-Template'
        )]
        [Alias('net')]
        [string]
        $NetworkName
        ,
        <# Set Nubmer of vCPU, default value is 4 vCPU.
        #>
        [Parameter(
            Mandatory = $false,
            Position = 6
        )]
        [Alias('cpu')]
        [int]
        $NumCpu = '4'
        ,
        <# Set RAM in GB, default value is 8 GB.
        #>
        [Parameter(
            Mandatory = $false,
            Position = 7
        )]
        [Alias('ram')]
        [int]
        $MemoryGB = '8'
        ,
        <# Set HDD size in GB, default values is 100 GB.
        #>
        [Parameter(
            Mandatory = $false,
            Position = 8
        )]
        [Alias('hdd')]
        [int]
        $DiskGB = '100'
        ,
        <# Set Disk Storage Format, default value is Thin. Please leave it default.       
        #>
        [Parameter(
            Mandatory = $false,
            Position = 9
        )]
        [Alias('hddFormat')]
        [string]
        $DiskStorageFormat = 'Thin'
        ,
        <# Set Operating System Type. Default value is windows9Server64Guest, this is for Windows Server 2016 and newer.
        You can list all possible values with [VMware.Vim.VirtualMachineGuestOsIdentifier].GetEnumValues().
        #>
        [Parameter(
            Mandatory = $false,
            Position = 10
        )]
        [Alias('os')]
        [string]
        $GuestId = 'windows9Server64Guest'
        ,
        <# Set -VCconnect parameter if you need connect to Vcenter
        #>
        [Parameter(
            Mandatory = $false,
            Position = 11
        )]
        [Alias('connect')]
        [switch]
        $VCconnect
        ,
        <# Set -StartVM parameter if VM should be run after create
        #>
        [Parameter(
            Mandatory = $false,
            Position = 12
        )]
        [Alias('start')]
        [switch]
        $StartVM
        ,
        
        <# Name of the template to use as a baseline.
        #>
        [Parameter(
            Mandatory = $true,
            ParameterSetName = 'template'
        )]
        [string]
        $TemplateName
        ,
        <# Password of local administrator.
        #>
        [Parameter(
            Mandatory=$true,
            ParameterSetName = 'template'
        )]
        [string]
        $AdminPass
        ,
        <# Credentials to be used for domain join. 
            PSCredential object.
            User is asked to provide these if this parameter is not used.
        #>
        [Parameter(
            ParameterSetName = 'template'
        )]
        [pscredential]
        $DomainCred = $null
        ,
        <# Domain where to join to.
        #>
        [Parameter(
            ParameterSetName = 'template'
        )]
        [string]
        $Domain = 'ucn.muni.cz'
        ,
        [Parameter(
            Mandatory=$true,
            ParameterSetName = 'template'
        )]
        [string]
        $IpAddress
        ,
        [Parameter(
            Mandatory=$true,
            ParameterSetName = 'template'
        )]
        [string]
        $SubnetMask
        ,
        [Parameter(
            Mandatory=$true,
            ParameterSetName = 'template'
        )]
        [string]
        $DefaultGateway
        ,
        <# Primary DNS server.
        #>
        [Parameter(
            Mandatory=$true,
            ParameterSetName = 'template'
        )]
        [string]
        $Dns1
        ,
        <# Secondary DNS server.
        #>
        [Parameter(
            Mandatory=$true,
            ParameterSetName = 'template'
        )]
        [string]
        $Dns2
        ,
        [Parameter(
            ParameterSetName = 'template'
        )]
        [string]
        $Organization = 'MasarykUniversity'
        ,
        [Parameter(
            ParameterSetName = 'template'
        )]
        [string]
        $OwnerFullName = 'Intitute of Computer Science'
        ,
        <# see https://www.vmware.com/support/developer/windowstoolkit/wintk40u1/html/New-OSCustomizationSpec.html
        #>
        [Parameter(
            ParameterSetName = 'template'
        )]
        [string]
        $TimeZoneCode = '095'
    )

    $ErrorActionPreference = "Stop"

    if ($VCconnect.IsPresent)
    {
        # Connect to Vcenter, fill credentials
        Connect-VIServer -Server $Vcenter -Protocol https    
    }

    # Create New VM
    if ( $PSCmdlet.ParameterSetName -eq 'template') {
        if ($DomainCred -eq $null) {
            $DomainCred = Get-Credential -Message "Credentials to be used for domain join:"
        }

        $template = Get-Template -Name $TemplateName

        $spec
        try {
            $spec = New-OSCustomizationSpec `
                    -Name "$Name-tempSpec" `
                    -OSType windows `
                    -ChangeSid `
                    -FullName $OwnerFullName `
                    -OrgName $Organization `
                    -TimeZone $TimeZoneCode `
                    -Domain $Domain `
                    -DomainCredentials $DomainCred `
                    -AdminPassword $AdminPass `
                    -NamingScheme fixed `
                    -NamingPrefix $Name
        } catch {
            Write-Host "Error while creating new specification: " -ForegroundColor Red
            $_
            Remove-OSCustomizationSpec $spec -Confirm:$false
            return
        }
        
        try {
            Get-OSCustomizationNicMapping -OSCustomizationSpec $spec | `
                Set-OSCustomizationNicMapping `
                    -IpMode UseStaticIP `
                    -IpAddress $IpAddress `
                    -SubnetMask $SubnetMask `
                    -DefaultGateway $DefaultGateway `
                    -Dns $Dns1, $Dns2
        } catch {
            Write-Host "Error while setting up network: " -ForegroundColor Red
            $_
            Remove-OSCustomizationSpec $spec -Confirm:$false
            return
        }
        
        try {
            $vm = New-VM `
                -Name $Name `
                -Datastore $Datastore `
                -Location $Location `
                -ResourcePool $ResourcePool `
                -DiskStorageFormat $DiskStorageFormat `
                -Template $template `
                -OSCustomizationSpec $spec

            $vm | Set-VM -NumCpu $NumCpu -MemoryGB $MemoryGB -Confirm:$false
            # toto nejde lebo VMwaru jebe.
 #          Get-HardDisk -VM $vm | Set-HardDisk -CapacityGB $DiskGB -Confirm:$false
        } catch {
            Write-Host "Error while creating the VM: " -ForegroundColor Red
            $_
            Remove-OSCustomizationSpec $spec -Confirm:$false
            Get-VM -Name $Name | Remove-VM
            return
        }

        Remove-OSCustomizationSpec $spec -Confirm:$false
    } else {
        New-VM `
            -Name $Name `
            -Datastore $Datastore `
            -Location $Location `
            -ResourcePool $ResourcePool `
            -NetworkName $NetworkName `
            -NumCpu $NumCpu `
            -MemoryGB $MemoryGB `
            -DiskGB $DiskGB `
            -DiskStorageFormat $DiskStorageFormat `
            -CD `
            -GuestId $GuestId 
    }

    if ($StartVM.IsPresent)
    {
        # Run VM
        Start-Sleep 5
        Get-VM -Name $Name | Start-VM
    }

}