﻿<#
.Synopsis
   Function for remove Backup Tag from VM in VMware.
.DESCRIPTION
   Function for remove Backup Tag from VM in VMware.
   VM Name could not be empty. It should delete or replace all machines. Edit this script carefully!
.NOTES
   Version:        0.1
   Author:         Ondrej Buday
   Creation Date:  5.9.2019
   Purpose/Change: 0.1 Modul was created
.EXAMPLE
   Remove-WinVMBackubTag -VM server1
.EXAMPLE
   Remove-WinVMBackubTag -VM server1 -Force
.EXAMPLE
   Remove-WinVMBackubTag -VM server1, server2, server3
#>

function Remove-WinVMBackubTag {
    [CmdletBinding()]
    Param
    (
        <# Set VM/VMs Name - Multimple values are possible
           Command with empty VM parameter is not allowed.
        #>
        [Parameter(
            Mandatory = $true
        )]
        [Alias('vmname')]
        [string[]]
        $VM
        ,
        # Force delete tag
        [Parameter(
            Mandatory = $false
        )]
        [Alias('f')]
        [switch]
        $Force
    )

    $Confirm = $true
        if ($Force.IsPresent) {
        $Confirm = $false
    }

    # This check is not necessary, because mandatory is $true, but... ;-)
    if (!$VM) {
        Write-Host "VM parameter could not be empty!"
    }
    else {
        # Get all Tags for VM
        $WinVMTags = (Get-TagAssignment -Entity $VM -Category 'Backup Scenario')

        foreach ($Tag in $WinVMTags)
        {
            Remove-TagAssignment -TagAssignment $Tag -Confirm:$Confirm
        }

        if (!$WinVMTags) {
            Write-Host "No backup tag has been found on VM" $VM "."
        }
    }
}