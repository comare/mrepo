﻿<#
.Synopsis
    Function for adding and removing HHD with help scripts
.DESCRIPTION
    Function for adding and removing HHD with help scripts
    Now only for DIS DOMAIN!!!!
.NOTES
    Version:        0.1 Beta
    Author:         Martin Kotlik
    Creation Date:  28.09.2019
    Purpose/Change: 0.1 Modul was created
.EXAMPLE
    Set-HDDtools -action add -vm server
#>

#Pridat disk do UCN a napsat fukci pro obe domeny a take u skriptu pridat add a remove.
#Kontrola zda je powershell pripojen k vsphere
function Set-HDDtools {
    [CmdletBinding()]
    param (
        # add or remove disk parametr
        [Parameter(Mandatory = $true)]
        [ValidateSet(,'add','remove')]
        [string] $action
        ,
        # virtual machine in which disk will be added or removed
        [Parameter(Mandatory = $true)]
        [string] $vm
    )

    $VMInstance = Get-VM -Name $vm

    if ($action -eq 'add')  {
        New-HardDisk -Persistence Persistent -VM $VMInstance -DiskPath "[storw-p2-l1-r5-images] win-scripts-tools.vmdk" -Confirm:$false
    }

    if ($action -eq 'remove')  {
        $hdds = Get-HardDisk -VM $VMInstance 
        $hdd = $hdds[-1]
        Remove-HardDisk -HardDisk $hdd -Confirm:$false
    }
}
