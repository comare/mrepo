﻿<#
.Synopsis
   Function for configure Backup Tag to VM in HA VMware.
.DESCRIPTION
   Function for configure Backup Tag to VM in HA VMware.
   VM Name could not be empty. It should delete or replace all machines. Edit this script carefully!
.NOTES
   Version:        0.1 Beta
   Author:         Ondrej Buday
   Creation Date:  5.9.2019
   Purpose/Change: 0.1 Modul was created
.EXAMPLE
   
#>

function Set-WinVMBackupTag {
    [CmdletBinding()]
    Param
    (
        <# Set VM/VMs Name - Multimple values are possible
           Command with empty VM parameter is not allowed.
        #>
        [Parameter(
            Mandatory = $true
        )]
        [Alias('vmname')]
        [string[]]
        $VM
        ,
        <# Set New backup tag.
           You can list All possible Windows backup tags with: (Get-Tag -Category 'Backup scenario' | Where-Object -Property Name -Like *_W_*).Name
        #>
        [Parameter(
            Mandatory = $true
        )]
        [Alias('tag')]
        [ValidateSet('P_W_1_system','P_W_1_full')] #,'P_W_1_D_0_1','P_W_1_D_0_2','DIS_W_1_system','DIS_W_1_full')]
        [string]
        $BackupTag
        ,
        <# If -Replace parameter is preset, existing Backup Tag will be replaced by new one
        #>
        [Parameter(
            Mandatory = $false
        )]
        [switch]
        $Replace
    )

    # This check is not necessary, because mandatory is $true, but... ;-)
    if (!$VM) {
        Write-Host "VM parameter could not be empty!"
    }
    else {
        # Get all Backup tags for VM/VMs
        $WinVMTags = (Get-TagAssignment -Entity $VM -Category 'Backup Scenario')

        ######### Nemám to ošetřené pro více serverů zároveň. Aktuální chování - pokud jeden server něco, tak všechny. Udělat, až bude čas !!!!!!!!!!! :-)
        # Check if any tag exist
        if (!$WinVMTags) {
            New-TagAssignment -Entity $VM -Tag $BackupTag
            Write-Host "VM" $VM "has been configured to backup tag" $BackupTag
        }
        else {
            foreach ($Tag in $WinVMTags) {    
                if ($Replace.IsPresent) {
                    Remove-TagAssignment -TagAssignment $Tag -Confirm:$false
                    New-TagAssignment -Entity $VM -Tag $BackupTag
                    Write-Host "Backup tag" $Tag.Tag.Name "has been replaced to VM" $VM "with backup tag" $BackupTag
                }
                else {
                    ######### Opravit. Vypíše ikdyž jenom jeden stroj tag obsahuje a nic se neprovede 
                    Write-Host "VM" $VM "already has been configured with backup tag" $Tag.Tag.Name "and has not been replaced. Please use -Replace parameter for replace this backup tag."
                }

            }
        }   

    }
}