<#
	.NOTES
		Author  : Adam Dobias
		Version : 0.1
		Created : 2019/08/21

    .DESCRIPTION 
        Creates new OU for given server (naming convention: <server_name>OU).
        Moves server there.
        Links default firewall policy.
        Creates custom FW policy and links it there if necessary.

	.EXAMPLE

    .EXAMPLE
    
    .PARAMETER Name
        Name of the server	
        
    .PARAMETER DC
        If the server is a domain controller.

    .PARAMETER Cluster
        If the server is a part of a cluster.

    .PARAMETER ClusterName
        Name of the cluster. Use in conjunction with -Cluster switch.

    .PARAMETER FWRules
        List of firewall rules.
        Totu este musim porozmyslat ako to spravit presne
    
    .PARAMETER DefaultFWPolicy
        Default Firewall policy which chould be applied

    .PARAMETER OUName
        Name of the OU where the computer should be moved. Default is (hostname)OU.
    
#>


function Set-WinVMFwOu {
    param (
        [Parameter(Mandatory=$true)]
        [string]$Name,
        [string]$Domain = 'ucn.muni.cz',
        [string]$SourceDomain = 'ucn.muni.cz', 
        [Parameter(ParameterSetName = 'DC')]
        [switch]$DC = $false,
        [Parameter(Mandatory = $true, ParameterSetName = 'Cluster')]
        [switch]$Cluster = $false,
        [Parameter(Mandatory = $true, ParameterSetName = 'Cluster')]
        [switch]$ClusterName,

        [string]$DefaultRestrictedGroupsPolicy = 'UCN, Servers, CUSTOM, Template, Restricted groups',
        [string]$DefaultFWPolicy = 'UCN, Servers, SECURITY, Firewall, Template',
        [string]$FWRules,
        [string]$OUName = "",
        [string]$ToExtisting = ""
        
    )
    
    $ErrorActionPreference = "Stop"

    if ([string]::IsNullOrWhiteSpace($OUName)) {
        $OUName = ('{0}OU' -f $Name)
    }

    $c = Get-ADComputer $Name -Server $Domain

    $domainDNSuffix = ''

    $domain.Split('.') | % {
        $domainDNSuffix += ",DC=$_"
    }


    <#
            Je to potrebne vytvarat postupne? ano treba.
    #>
    if (!([string]::IsNullOrWhiteSpace($ToExtisting))) {
        $target = $ToExtisting
    } elseif ($PSCmdlet.ParameterSetName -eq 'DC') {
        if (!($Name -match '.*(\d+)$')) {
            Write-Error "Expecting name if DC in format 'server-nameXX', e.g. 'ucn-server4'"
        }

        $OUName = $Matches[1]
        
        $parent = ('OU=Domain Controllers{0}' -f $domainDNSuffix)
        $target = ('OU={0},{1}' -f $OUName, $parent)
        "creating new AD OU: $target"
        
        New-ADOrganizationalUnit -Name $OUName -Path $parent -Server $Domain

    } elseif ($PSCmdlet.ParameterSetName -eq 'Cluster') {
        $parent = $domainDNSuffix
        $target = ('OU=Servers_SCCM,{0}' -f $parent)
        if (!(Get-ADOrganizationalUnit -Filter "distinguishedname -eq $target")) {
            New-ADOrganizationalUnit -Name 'Servers_SCCM' -Path $parent -Server $Domain
        }
        $parent = $target
        $target = ('OU={0},{1}' -f $OUName, $target)

        #$target = ('OU={0},{1}' -f $OUName, $target)
        if (!(Get-ADOrganizationalUnit -Filter "distinguishedname -eq $target")) {
            New-ADOrganizationalUnit -Name $OUName-Path $parent -Server $Domain
        }
    } else {
        $parent = $domainDNSuffix
        $target = ('OU=Servers_SCCM,{0}' -f $parent)
        if (!(Get-ADOrganizationalUnit -Filter "distinguishedname -eq $target")) {
            New-ADOrganizationalUnit -Name 'Servers_SCCM' -Path $parent -Server $Domain
        }
        $parent = $target
        $target = ('OU={0},{1}' -f $OUName, $target)

        #$target = ('OU={0},{1}' -f $OUName, $target)
        New-ADOrganizationalUnit -Name $OUName -Path $parent -Server $Domain
    }

    if ($Domain -ne $SourceDomain) {
        $newFwName = $DefaultFWPolicy.Replace($DefaultFWPolicy.Split(',')[0], $Domain.Split('.')[0].ToUpper() + ", $Name")
        $newRgName = $DefaultRestrictedGroupsPolicy.Replace($DefaultRestrictedGroupsPolicy.Split(',')[0], $Domain.Split('.')[0].ToUpper() + ", $Name")
    } else {
        $newFwName = $DefaultFWPolicy
        $newRgName = $DefaultRestrictedGroupsPolicy
    }

    #$target = ('OU={0},{1}' -f $OUName, $target)

    Copy-GPO -SourceName $DefaultFWPolicy -SourceDomain $SourceDomain `
                -TargetName $newFwName -TargetDomain $Domain

    Copy-GPO -SourceName $DefaultRestrictedGroupsPolicy -SourceDomain $SourceDomain `
                -TargetName $newRgName -TargetDomain $Domain

    
    "linking $newFwName to $target"
    New-GPLink -Name $newFwName -Target $target -Domain $Domain
    "linking $newFwName to $target"
    New-GPLink -Name $newRgName -Target $target -Domain $Domain

    Move-ADObject -Identity $c.DistinguishedName -TargetPath $target -Server $Domain

    

    # TODO: Process custom FW rules

}