# Check RSAT AD POwershel Tolls
if ((Get-WindowsFeature RSAT-AD-PowerShell).Installed -eq $false) {
    Add-WindowsFeature RSAT-AD-PowerShell
}
# Import AD module
Import-Module -Name ActiveDirectory

# Add our local PS repository
try {
    if (Get-PSRepository -Name Winadm) {
        Unregister-PSRepository -Name Winadm
    }
    Register-PSRepository -Name Winadm -SourceLocation \\ucn.muni.cz\Network\git_repo\PSRepository -InstallationPolicy Trusted
} catch {
    Write-Host "Cannot load PSrepository on UCN DFS share. Check if folder is accessible" -ForegroundColor Red
}

# Add ICS_ServerTools and VMwarePowerCLI modules
try{
    Write-Host "Importing VMware.PowerCLI Version 10.2..."
    Import-module -Name VMware.PowerCLI -RequiredVersion 10.2.0.9372002

    if (!(Get-Module -Name ICS_ServerTools -ListAvailable)) {
        Write-Host "Installing ICS_ServerTools"
        Install-Module -Name ICS_ServerTools -Force
    }
    Write-Host "Checking new version of ICS_ServerTools module on repository..."
    Update-Module -Name ICS_serverTools
    Write-host "Importing module ICS_ServerTools..."
    Import-Module -Name ICS_ServerTools -Force
} catch {
    Write-Host "Modules VMware.PowerCLI ver. 10.2 or ICS_ServerTools canot be imported. Not found maybe" -ForegroundColor Red
}

# Set location of commandline
Set-Location C:\Users\kotlikadm\Documents\WindowsPowershell\