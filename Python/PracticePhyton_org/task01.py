from datetime import date

actual_year = date.today().year

name = input("Insert your name: ")
age = int(input("Insert your age: "))

year_of_100 = str(actual_year + (100 - age))

msg = "{0} you will be 100 years old at year {1} \n".format(name, year_of_100)

times = int(input("How many times do you want to print it?: "))

print(times * msg)