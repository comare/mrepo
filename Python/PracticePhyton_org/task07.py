a = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]

even_list = [i for i in a if i % 2 == 0]
print(even_list)

even_list2 = list(filter(lambda x: x % 2 == 0, a))
print(even_list2)

def filterbyeven(seq):
   for i in seq:
       if i % 2 == 0:
           yield i

even_list3 = list(filterbyeven(a))
print(even_list3)
