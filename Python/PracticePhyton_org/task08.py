def compare(p1, p2):
    if p1 == p2:
        print("Same symbols - TIE")
    elif p1 == "paper" and p2 == "rock":
        print("Player1 wins")
    elif p1 == "rock" and p2 == "scissors":
        print("Player1 wins")
    elif p1 == "scissors" and p2 == "paper":
        print("Player1 wins")
    else:
        print("Player2 wins")

print("!!!PAPER, ROCK, SCISSORS GAME!!!")
command = "start"
while command != "no":
    player1 = str(input("Player 1: Enter your choice:"))
    player2 = str(input("Player 2: Enter your choice:"))

    compare(player1.lower(), player2.lower())

    command = input("Do you want to play again?(yes / no): ")