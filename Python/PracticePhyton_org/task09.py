import random

random.seed()
number = random.randint(1, 9)

attempts = 0

while True:
    match = input("Guess the number: ")
    attempts += 1
    if match.lower() == "exit":
        print("Exiting GAME")
        break
    match = int(match)
    if match == number:
        print("You have guest the right number - {}".format(number))
        print("You need {} attempts to guess the number".format(attempts))
        break
    elif match > number:
        print("Too high")
    else:
        print("Too low")


