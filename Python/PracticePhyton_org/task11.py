num = int(input("Enter number to check: "))
# Find all x in range 1 to entered number which can module entered number without remainder
divisors = [x for x in range (1,num+1) if num % x == 0]

if num == 1:
    print("{} is prime number".format(num))
if (len(divisors)) <= 2:
    print("{} is prime number".format(num))
else:
    print("{} is NOT prime number".format(num))