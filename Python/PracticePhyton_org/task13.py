def fibonnaci(pos):
    fib = []
    for i in range(0, pos):
        if i == 0 or i == 1:
            fib.append(1)
        else:
            fib.append(fib[-2]+fib[-1])
    return fib

positions = int(input("Plese specify how many positions of Fibonnaci number do you want: "))
print(fibonnaci(positions))

""" Without list
def fibonnaci(pos):
    print("1, ", end="")
    if pos == 1:
        return
    n1 = 0
    n2 = 1
    while pos > 0:
        sum = n1 + n2
        n1, n2 = n2, sum
        print("{}, ".format(sum), end = "")
        pos -= 1

positions = int(input("Plese specify how many positions of Fibonnaci number do you want: "))

fibonnaci(positions)
"""