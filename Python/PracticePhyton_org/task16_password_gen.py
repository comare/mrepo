import random
import string

'''
Generate password with given length. Lowercase -> Replaced with some Uppercase 
-> Replaced with one digit and one punctuation
'''
def generatePass(length_):
    random.seed()
    password = ""

    for i in range(1, length_+1):
        password += random.choice(string.ascii_lowercase)

    for i in range(1, (length_+1)//2):
        password = password.replace(password[random.randint(0, length_-1)], random.choice(string.ascii_uppercase))

    password = password.replace(password[random.randint(0, length_-1)], random.choice(string.digits))

    password = password.replace(password[random.randint(0, length_-1)], random.choice(string.punctuation))
    return password

again = "y"
print("PASSWORD GENERATOR")
while again == "y":
    length = int(input("Password length: "))
    print("---------------------------------")
    print(generatePass(length))
    print("---------------------------------")
    again = (input("Do you want to generate another password? (y / n): "))