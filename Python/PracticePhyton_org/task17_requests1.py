"""
This python script decode-a-web-page
"""
import requests
from bs4 import BeautifulSoup

url = 'https://www.svethardware.cz/'
r = requests.get(url)
soup = BeautifulSoup(r.text, 'html.parser')

list_of_article_divs = soup.find_all(class_= "article")

for article_div in list_of_article_divs:
    article_text = article_div.h2
    if article_text is not None:
        print("Článek - ", article_text.string)