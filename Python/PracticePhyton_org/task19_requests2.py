"""
This python script decode-a-web-page
"""
import requests
from bs4 import BeautifulSoup

url = 'https://www.vanityfair.com/style/society/2014/06/monica-lewinsky-humiliation-culture'
link = requests.get(url).text
soup = BeautifulSoup(link, 'html.parser')

paragraphs = soup.article.find_all('p')

for p in paragraphs:
    print(p.get_text())
