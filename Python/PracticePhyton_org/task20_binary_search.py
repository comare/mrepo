def compare(number, list):
    if list.count(number) >= 1:
        return True
    else:
        return False

def getLengths(list):
    length = len(list)
    middle = int(length / 2)
    return length, middle
    

def binarySearch(number, list):
    length, middle = getLengths(list)
    while length > 1:
        if number == list[middle]:
            return True
        elif list[middle] > number:
            list = list[:middle]
        else:
            list = list[middle:]
        length, middle = getLengths(list)
        print(list)
    return False

if __name__=="__main__":
    list = [2,8,12,44,56,70,88,93,108,159,195,207]
    number = int(input("Please enter number: "))

    print("-------------------------------------------------")
    print("Searching in list by compare function")
    result = compare(number, list)
    print("The number {0} inside list {1}: {2}".format(number, list, result))
    print("-------------------------------------------------")
    print("Searching in list by binarySearch function")
    result = binarySearch(number, list)
    print("The number {0} inside list {1}: {2}".format(number, list, result))