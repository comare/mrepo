def char_count(text, char):
    count = 0
    for i in text:
        if i == char:
            count += 1
    return count

with open("test.txt", "r") as file:
    text = file.read()

for char in "abcdefghijklmnopqrstuvwxyz":
    perc = 100 * char_count(text, char) / len(text)
    print("{0} - {1}%".format(char,round(perc,2)))


""" My solution to the problem - count every char in file and store them to dictionary
with open("test.txt", "r") as file:
    text = file.read()
characters = {}
length = len(text)
for i in text:
    countOfChar = characters.get(i)
    if countOfChar == None:
        countOfChar = 1
    else:
        countOfChar += 1
    characters[i] = countOfChar
for i in characters:
    percent = round(((characters[i] / length) * 100),2)
    print("Character {0} is there {1} times and it is {2} percent".format(i, characters[i], percent))
"""