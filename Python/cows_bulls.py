import random

def getRandom():
    digits = list(range(10))
    random.shuffle(digits)
    number = digits[:3]
    print(number)
    return number

def getGuess():
    guess = input("What is your guess?: ")
    guess = (list(map(int, guess)))
    return guess

def getClues(number, guess):
    clues = []
    for i in range(3):
        if number[i] == guess[i]:
            clues.append("Match")
        elif guess[i] in number:
            clues.append("Close")
    if clues == []:
        clues.append("Nope")
    return clues


number = getRandom()
print("Number of 3 digits is generated...")
guess = []

while (guess != number):

    guess = getGuess()
    clues = getClues(number, guess)

    for clue in clues:
        print(clue)

    if (guess == number):
        print("Good job!")
