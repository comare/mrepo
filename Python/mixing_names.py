def setNameSurname(whole_name):
    if whole_name.count(" ") == 1:
        name_split = whole_name.split(" ")
    else:
        raise ValueError("Only names with two substrings allowed")
    return name_split[0], name_split[1]

def maxLength(name, surname):
    if len(name) <= len(surname):
        return len(name)
    else:
        return len(surname)

def print_mixNames(name, surname):
    for i in range(1, maxLength(name, surname)):
        print(name[:i] + surname[i:], surname[:i] + name[i:])

whole_name = ""
while whole_name != "exit":
    print("MIXING NAMES (for exit type exit)")
    whole_name = str(input("Please enter your name: "))
    name, surname = setNameSurname(whole_name)
    print_mixNames(name, surname)