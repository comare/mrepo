"""
This module generate complex password of given length
"""
import random
import string

def generate_pass(length_):
    """
    Generate password with given length. Lowercase -> Replaced with some Uppercase
    -> Replaced with one digit and one punctuation
    """
    random.seed()
    password = ""

    for i in range(1, length_ + 1):
        password += random.choice(string.ascii_lowercase)

    for i in range(1, (length_ + 1) // 2):
        password = password.replace(password[random.randint(0, length_ - 1)],  
                                    random.choice(string.ascii_uppercase), 1)

    password = password.replace(password[random.randint(0, length_ - 1)], 
                                random.choice(string.digits), 1)

    password = password.replace(password[random.randint(0, length_ - 1)], 
                                random.choice(string.punctuation), 1)
    return password


again = "y"
print("PASSWORD GENERATOR")
while again == "y":
    length = int(input("Password length: "))
    print("---------------------------------")
    print(generate_pass(length))
    print("---------------------------------")
    again = (input("Do you want to generate another password? (y / n): "))
