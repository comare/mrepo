# Add detection clausule for x64 version
$cla1=New-CMDetectionClauseFile -FileName "chrome.exe" -PropertyType Version -ExpectedValue "87.0.4280.88" -ExpressionOperator IsEquals -Path "%ProgramFiles(x86)%\Google\Chrome\Application" -Value
$cla2=New-CMDetectionClauseFile -FileName "chrome.exe" -PropertyType Version -ExpectedValue "87.0.4280.88" -ExpressionOperator IsEquals -Path "%ProgramFiles%\Google\Chrome\Application" -Value
$logic1=$cla1.Setting.LogicalName
$logic2=$cla2.Setting.LogicalName
Set-CMMSiDeploymentType -ApplicationName "Google Chrome 87.0.4280.88" -DeploymentTypeName "Google Chrome Silent Install x64" -AddDetectionClause $cla1,$cla2 -GroupDetectionClauses $logic1,$logic2 -DetectionClauseConnector @{LogicalName=$logic2;Connector="or"}

# Add detection clausule for x86 version - need to be duplicated!
$cla1=New-CMDetectionClauseFile -FileName "chrome.exe" -PropertyType Version -ExpectedValue "87.0.4280.88" -ExpressionOperator IsEquals -Path "%ProgramFiles(x86)%\Google\Chrome\Application" -Value
$cla2=New-CMDetectionClauseFile -FileName "chrome.exe" -PropertyType Version -ExpectedValue "87.0.4280.88" -ExpressionOperator IsEquals -Path "%ProgramFiles%\Google\Chrome\Application" -Value
$logic1=$cla1.Setting.LogicalName
$logic2=$cla2.Setting.LogicalName
Set-CMMSiDeploymentType -ApplicationName "Google Chrome 87.0.4280.88" -DeploymentTypeName "Google Chrome Silent Install x86" -AddDetectionClause $cla1,$cla2 -GroupDetectionClauses $logic1,$logic2 -DetectionClauseConnector @{LogicalName=$logic2;Connector="or"}

# Add-CMMsiDeploymentType -AddDetectionClause $cla1,$cla2,$cla3 -ApplicationName "app" -DeploymentTypeName "dt" -InstallCommand "mycommand" -ContentLocation “\\server\sources\Orca.msi” -GroupDetectionClauses $logic1,$logic2 -DetectionClauseConnector {LogicalName=$logic2;Connector="or"},{LogicalName=$logic3;Connector="or"}
# Set-CMScriptDeploymentType -ApplicationName Google Chrome 87.0.4280.88 -DeploymentTypeName Google Chrome Silent Install x64 -AddDetectionClause Version