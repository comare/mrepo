﻿$members = Get-ADGroupMember -Identity Wplace-920310
$members += Get-ADGroupMember -Identity ukb-ts
Write-Host "Exporting group:" | Out-File -FilePath "C:\temp\test.txt"
foreach ($member in $members){
    $user = Get-ADUser -Identity $member.name -Properties *
    $user.EmailAddress | Out-File -FilePath "C:\temp\test.txt" -Append
}

#Send-MailMessage -from "Winadm <winadm@ics.muni.cz>" `
#                       -to "TSUKBuser <kotlik@ics.muni.cz>" `
#                       -subject "Nedostupnost terminálových serverů" `
#                       -BodyAsHtml "Dobrý den,<br/>Ve středu 19.6.2019 se budou v rámci rušení části infrastruktury přesouvat i nějaké virtuální servery. <br/>Z toho důvodu nebude po dobu cca 3 hodin dostupný terminálový server ts.ukb.muni.cz. Prosíme všechny,  aby se v tuto dobu nepřipojovali a také se v průběhu zítřka odhlásili, pokud jsou na serverech přihlášení.<br/> V případě dotazů pište na winadm@ics.muni.cz<br/><br/>S pozdravem,<br/>Martin Kotlík" `
#                       -Encoding ([System.Text.Encoding]::UTF8)`
#                       -smtpServer relay.muni.cz 
#Write-Host "Email odeslán"