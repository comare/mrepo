# Getting server info from domain/domains.
function Get-ServerInfoList 
    (
        [ValidateScript({get-adcomputer -identity $_})]
        [string[]]$ServerNames = (Get-ADComputer -Filter *).Name
    )
{
    Class Server {
        [string]$ComputerName
        [string]$IPAddress
        
        Server($ServerLoaded) {
            $this.ComputerName = $ServerLoaded.Name
            $this.IPAddress = $ServerLoaded.IPV4Address
        }
    }
    
    ForEach($ServerName in $ServerNames) {
        $ServerObject = Get-ADComputer -Identity $ServerName -Properties Name,IPV4Address
        [array]$ServerList += New-Object Server $ServerObject
    }
    $ServerList
}

Get-ServerInfoList -ServerNames ucn-server3