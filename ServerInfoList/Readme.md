# Server Info List
My list for server info approaches
1. Collect data from one server with invoke-command, store data in db, and app on same server
    - Problem with our enviroment - different domains, different subnets, servers not in domain
2. DB and webapp on one server and data send from all machines by scheduled job
    - Who to verify that all data are collected?

## Problems
- Versions of list for each day - search through history is required

## Progress of project in steps approach 1
1. Collect data from servers in all forests by Powershell from ucn-server4
    - Try to collect data in threads to speed up process
    - Mark servers and maybe content as autoload or manualy created
2. Save data to database (any - probably MSSQL can be good to learn something) on some server - not necessarily ucn-server4
    - Make some system to store old data, removed servers atd.
3. Make application (choose some language) to load data from database
    - Edit app to let some users to change and add data in database
    - Enable app to add additional servers to database


## Basic setting and variables
- Name
- IP
- Domain
- Last Logon
- Platform
- OS
- OU Path
- Maintainer
- Customer
- Priority
- AD Description
- Description