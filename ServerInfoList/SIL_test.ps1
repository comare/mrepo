$job = Invoke-Command -ComputerName (Get-ADComputer -Filter *).Name -ScriptBlock { (Get-ChildItem C:\Windows\System32 -Filter ntdll*).VersionInfo |
    Select-Object InternalName,FileDescription,FileVersion } -AsJob
$result = $job | wait-job | Receive-job
$job | Remove-Job
$result | Select-Object PSComputerName,InternalName,FileVersion,FileDescription