module "aadc" {
  source = "./server"
  
  network       = var.network_id
  subnet        = var.contoso-subnet
  name          = "aadc"
  flavor_name   = "standard.2core-16ram"
  image         = var.server_standard-image
  security_groups = ["default", "RDP"]
}
