// First AD server

// Port
resource "openstack_networking_port_v2" "contoso-ad1-port" {
  name           = "contoso-ad1-port"
  network_id     = var.network_id
  admin_state_up = "true"

  fixed_ip {
    subnet_id = var.contoso-subnet
  }
}

// Instance
resource "openstack_compute_instance_v2" "windows-contoso-ad1" {
  name            = "ad-1"
  flavor_name     = "standard.medium"
  security_groups = ["default"]
  config_drive    = "true"

  metadata = {
    admin_pass = "ics_123456"
  }

  network {
    port = openstack_networking_port_v2.contoso-ad1-port.id
  }

  block_device {
    uuid                  = var.server_core-image
    source_type           = "image"
    destination_type      = "volume"
    volume_size           = 80
    boot_index            = 0
    delete_on_termination = true
  }
}

// Second AD server

// Port
resource "openstack_networking_port_v2" "contoso-ad2-port" {
  name           = "contoso-ad2-port"
  network_id     = var.network_id
  admin_state_up = "true"

  fixed_ip {
    subnet_id = var.contoso-subnet
  }
}

// Instance
resource "openstack_compute_instance_v2" "windows-contoso-ad2" {
  name            = "ad-2"
  flavor_name     = "standard.medium"
  security_groups = ["default"]
  config_drive    = "true"

  metadata = {
    admin_pass = "ics_123456"
  }

  network {
    port = openstack_networking_port_v2.contoso-ad2-port.id
  }

  block_device {
    uuid                  = var.server_core-image
    source_type           = "image"
    destination_type      = "volume"
    volume_size           = 80
    boot_index            = 0
    delete_on_termination = true
  }
}