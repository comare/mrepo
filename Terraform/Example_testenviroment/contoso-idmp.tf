module "idmp" {
  source = "./server"
  
  network       = var.network_id
  subnet        = var.contoso-subnet
  name          = "idmp"
  flavor_name   = "standard.medium"
  image         = var.server_core-image
  security_groups = ["default", "RDP"]
}

resource "openstack_networking_floatingip_v2" "idmp-floating" {
  pool = var.public-fip-pool
}

resource "openstack_compute_floatingip_associate_v2" "idmp-assoctiate" {
  floating_ip = openstack_networking_floatingip_v2.idmp-floating.address
  instance_id = module.idmp.instance_id
}
