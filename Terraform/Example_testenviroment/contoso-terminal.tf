// First terminal server

// Port
resource "openstack_networking_port_v2" "contoso-terminal-port" {
  name           = "contoso-terminal-port"
  network_id     = var.network_id
  admin_state_up = "true"

  fixed_ip {
    subnet_id = var.contoso-subnet
  }
}

resource "openstack_compute_instance_v2" "contoso-terminal-server" {
  name            = "terminal"
  flavor_name     = "standard.medium"
  security_groups = ["default", "DNS", "HTTPS", "PING", "RDP", "HTTPS (8080)"]
  config_drive    = "true"

  metadata = {
    admin_pass = "ics_123456"
  }

  network {
    port = openstack_networking_port_v2.contoso-terminal-port.id
  }

  block_device {
    uuid                  = var.server_standard-image
    source_type           = "image"
    destination_type      = "volume"
    volume_size           = 100
    boot_index            = 0
    delete_on_termination = true
  }
}

resource "openstack_networking_floatingip_v2" "contoso-fip_terminal" {
  pool = var.public-fip-pool
}

resource "openstack_compute_floatingip_associate_v2" "as_terminal" {
  floating_ip = openstack_networking_floatingip_v2.contoso-fip_terminal.address
  instance_id = openstack_compute_instance_v2.contoso-terminal-server.id
}