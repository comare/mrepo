module "fabrikam-ad-1" {
  source = "./server"
  
  network       = var.network_id
  subnet        = var.fabrikam-subnet
  name          = "ad-1"
  flavor_name   = "standard.small"
  image         = var.server_core-image
  security_groups = ["default", "RDP"]
}

module "fabrikam-ad-2" {
  source = "./server"
  
  network       = var.network_id
  subnet        = var.fabrikam-subnet
  name          = "ad-2"
  flavor_name   = "standard.small"
  image         = var.server_core-image
  security_groups = ["default", "RDP"]
}