module "fabrikam-terminal" {
  source = "./server"
  
  network       = var.network_id
  subnet        = var.fabrikam-subnet
  name          = "terminal"
  flavor_name   = "standard.medium"
  image         = var.server_standard-image
  security_groups = ["default", "DNS", "HTTPS", "PING", "RDP"]
}

resource "openstack_networking_floatingip_v2" "fabrikam-terminal-fip" {
  pool = var.public-fip-pool
}
resource "openstack_compute_floatingip_associate_v2" "fabrikam-terminal-assoctiate" {
  floating_ip = openstack_networking_floatingip_v2.fabrikam-terminal-fip.address
  instance_id = module.fabrikam-terminal.instance_id
}