module "powershell-proxy-1" {
  source = "./pwsh_proxy"
  
  network       = var.network_id
  subnet        = var.contoso-subnet
  name          = "psws-v2n1"
  flavor_name   = "standard.2core-16ram"
  image         = var.server_core-image
}
module "powershell-proxy-2" {
  source = "./pwsh_proxy"
  
  network       = var.network_id
  subnet        = var.contoso-subnet
  name          = "psws-v2n2"
  flavor_name   = "standard.2core-16ram"
  image         = var.server_core-image
}
module "powershell-proxy-3" {
  source = "./pwsh_proxy"
  
  network       = var.network_id
  subnet        = var.contoso-subnet
  name          = "psws-v2n3"
  flavor_name   = "standard.2core-16ram"
  image         = var.server_core-image
}
