module "fabrikam-sccm" {
  source = "./server"
  
  network       = var.network_id
  subnet        = var.fabrikam-subnet
  name          = "sccm"
  flavor_name   = "standard.medium"
  image         = var.server_standard-image
  security_groups = ["default", "HTTPS", "PING", "RDP"]
}