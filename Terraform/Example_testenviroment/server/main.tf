resource "openstack_networking_port_v2" "instance_port" {
  name           = "${var.name}-port"
  network_id     = var.network
  admin_state_up = "true"

  fixed_ip {
    subnet_id = var.subnet
  }
}

resource "openstack_compute_instance_v2" "instance" {
  name            = var.name
  flavor_name     = var.flavor_name
  security_groups = var.security_groups
  config_drive    = "true"


  metadata = {
    admin_pass = "ics_123456"
  }

  network {
    port = openstack_networking_port_v2.instance_port.id
  }

  block_device {
    uuid                  = var.image
    source_type           = "image"
    destination_type      = "volume"
    volume_size           = var.image_size
    boot_index            = 0
    delete_on_termination = true
  }

}