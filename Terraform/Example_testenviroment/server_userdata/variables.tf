variable "flavor_name" {
  description = "Instance type"
}
variable "name" {
  description = "Instance name"
}
variable "security_groups" {}

variable "image" {
  description = "Image"
}

variable "image_size" {
  default = 80
}

variable "subnet" {
  description = "Subnet"
}

variable "network" {
  description = "Network ID"
}

variable "user_data" {}






