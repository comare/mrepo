variable "network_name" {
  default = "auto_allocated_network"
}
variable "network_id" {
  default = "44754a02-27e3-4483-8250-edb911e652b3"
}
variable "fabrikam-subnet" {
  default = "ba7a750b-65ce-4505-965e-98e206b5ff9b"
}
variable "contoso-subnet" {
  default = "f6138b70-d94d-4eb1-aaf8-a98605a59d40"
}
variable "public-fip-pool" {
  default = "public-cesnet-78-128-251-GROUP"
}
variable "server_standard-image" {
  default = "aa61b4e3-5765-4d52-8970-32f5014ab40f"
}
variable "server_core-image" {
  default = "c15a900e-3a3a-43b8-9f88-663f014f7295"
}