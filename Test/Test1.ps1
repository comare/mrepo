﻿$customscript = "C:\Users\kotlikadm\Documents\WindowsPowershell\mrepo\Test\Test2.ps1"
$configfile = "C:\Users\kotlikadm\Documents\WindowsPowershell\mrepo\Test\config.json"

$config = Get-Content $configfile | ConvertFrom-Json

#Additional settings for server: Features, Roles, Config
Try {
    Write-Host "Launching custom script $customscript on server $($config.Server.Name) ..."
    $job = Invoke-Command -ComputerName $config.Server.Name -FilePath $customscript -ArgumentList $config -AsJob
    Wait-Job -Job $job
    Write-Host "Finished executing custom script. Writing results:"
    Receive-Job -Job $job
    Remove-Job -Job $job
} catch {
    Write-Host "Error during executing custom script"
}