# CUSTOM SCRIPT FOR SERVER
# This code will be launched at the end of Masterscript. It is specific for each server.
# Config JSON file is loaded from main script and you can use all variables from it.

Workflow Install-Custom {
    ##########################################################################
    # START of custom CODE for setting up server
    Write-Verbose "Before Restart"
    Restart-Computer -Wait
    Write-Verbose "After Restart"
    Start-Sleep -Seconds 5
    New-Item -Type Directory -Name Afterrestart -Path C:\
    
    # END of custom CODE for setting up server
    ##########################################################################
}