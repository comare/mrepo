# GIT Pull requests to all subfolders 1st and 2nd level in current folder

$directories = Get-ChildItem -Directory
foreach ($directory in $directories) {
    if ($directory.GetDirectories(".git").Exists) {
        Write-Host "--- Getting latest for $($directory.Name)" -ForegroundColor Green
        git -C $directory.FullName pull --all --recurse-submodules --verbose
    } else {
        $subdirectories = $directory.GetDirectories()
        foreach ($subdirectory in $subdirectories) {
            if ($subdirectory.GetDirectories(".git").Exists) {
                Write-Host "--- Getting latest for $($directory.Name)" -ForegroundColor Green
                Write-Host "------ \$($subdirectory.Name)" -ForegroundColor DarkGreen
                git -C $subdirectory.FullName pull --all --recurse-submodules --verbose
            }
        }
    }
}

<#
Get-ChildItem -Directory | 
    ForEach-Object { Write-Host "---- Getting latest for $_" -ForegroundColor Green | 
    git -C $_.FullName pull --all --recurse-submodules --verbose }
#>