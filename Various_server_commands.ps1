﻿# Vycisteni pameti powershellu za behu
Remove-Variable * -ErrorAction SilentlyContinue; Remove-Module *; $error.Clear(); Clear-Host

# Vyhledání uživatele s určitým sSamAccountName
Get-ADUser -Filter 'SamAccountName -like "*Proteomika-Mascot*"' | Format-Table Name,SamAccountName -A

# Vyhledání počítačů z určité OU
Get-ADComputer -Filter * -SearchBase "OU=m215,OU=Provozni odbor,OU=ZAM,OU=RECT,OU=WIN10,OU=INFRASTRUKTURA_SCCM,DC=staff,DC=ucn,DC=muni,DC=cz"

# Get all services that are stopped and should be running
Get-Service | Select-Object -Property Name,Status,StartType | Where-Object {$_.Status -eq "Stopped" -and $_.StartType -eq "Automatic"}

# Get certificate by some atribute / for example Thumbprint
Get-ChildItem -path 'Cert:\' -Recurse | Where-Object Thumbprint -like '*55545vabb*' | Select-Object *

# Get error logs from Application log
Get-EventLog -LogName Application -Newest 200 | Where-Object EntryType -eq Error | ft -wrap

# Execute scriptblock on list of servers
$servers = 'KUKKAT01','KUKKAT02','KUKKAT03','KUKPC85','KUKPC96'
foreach ($server in $servers) {
    Write-Output "Executing script on server $server"
    $session = New-PSSession -ComputerName $server
    Invoke-Command -Session $session -ScriptBlock {Restart-Computer -Force}
}
